#include "vBasicFighter.hpp"

int	vBasicFighter::width = 3;
int	vBasicFighter::height = 3;

vBasicFighter::vBasicFighter() : vEnemy() {}		/* private constructor */

vBasicFighter::vBasicFighter(int x, int y)
        : vEnemy(x, y, vBasicFighter::height, vBasicFighter::width)
{
    setShape();
    m_hp = 10;
    m_maxHP = 10;
    m_damage = 4;
    m_armor = 1;
    m_gunCoords = cIntPair(2, 1);
}

vBasicFighter::vBasicFighter(const vBasicFighter &copy) : vEnemy(copy) {
    *this = copy;
}

vBasicFighter::~vBasicFighter() {}

vBasicFighter	&vBasicFighter::operator=(const vBasicFighter &assign) {
    vEnemy::operator=(assign);
    return (*this);
}

void	vBasicFighter::setShape() {
    m_shape.clear();
    m_shape.push_back(L"{-}");
    m_shape.push_back(L"\\ /");
    m_shape.push_back(L" ˇ ");
}