/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gameEngine.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 18:05:01 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 18:05:04 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gameEngine.hpp"

gameEngine::gameEngine() : objManager(&global) {
	m_gameStatus = Startup;
	m_frameTimer = 0;
	m_gameTime = 120;
}

gameEngine::gameEngine(const gameEngine &) : objManager(NULL) {}	/* private constructor */

gameEngine::~gameEngine() {}

gameEngine& gameEngine::operator=(const gameEngine &) {				/* private assignment operator */
	return (*this);
}

bool		gameEngine::moveUser(int toX, int toY) {
	if (toX + vUser::height >= global.gameHeight()  || toY + vUser::width >= global.winWidth() || toX <= 0 || toY <= 0)
		return (false);
	vUser		*usr = objManager.user();
	for (int i = toX; i < toX + usr->getSize().x; i++) {
		for (int j = toY; j < toY + usr->getSize().y; j++) {
			int objectIndex = objManager.getIndexByCoords(cIntPair(i, j));
			if (objectIndex <= 0 || dynamic_cast<vBullet *>(objManager.getObjects()[objectIndex]))
				continue;
			m_gameStatus = Loose;
			return (false);
		}
	}
	usr->setCoords(toX, toY);
	return (true);
}

bool		gameEngine::moveEnemy(vEnemy *target) {
	if (!target)
		return (false);
	for (int i = 0; i < target->getSize().y; ++i) {
		cIntPair	checkCoords = cIntPair(target->getCoords().x + target->getSize().x, target->getCoords().y + i);
		if (objManager.getObjectByCoords(checkCoords))
			return (false);
	}
	target->setCoords(target->getCoords().x + 1, target->getCoords().y);
	return (true);
}

void		gameEngine::handleKey(int ch) {
	vUser	*user = objManager.user();

	switch (ch) {
		case KEY_UP:
			moveUser(user->getCoords().x - 1, user->getCoords().y);
			break;
		case KEY_DOWN:
			moveUser(user->getCoords().x + 1, user->getCoords().y);
			break;
		case KEY_LEFT:
			moveUser(user->getCoords().x, user->getCoords().y - 1);
			break;
		case KEY_RIGHT:
			moveUser(user->getCoords().x, user->getCoords().y + 1);
			break;
		case ' ':
			shootByUser();
			break;
		case 'q':
			m_gameStatus = Exiting;
			break;
		case 'p':
			m_gameStatus = Pause;
			break;
		default:
			break;
	}
}

void		gameEngine::createRandomEnemy() {
	vEnemy	*createdObject;
	int 	randY = (clock() % (global.winWidth() - vProFighter::width - 1)) + 1;		/* for correct positioning - do not conflict with frame */

	switch (clock() % 3) {
		case 0:

			createdObject = new vBasicFighter(-vBasicFighter::height, randY);
			break;
		case 1:
			createdObject = new vAdvancedFighter(-vAdvancedFighter::height, randY);
			break;
		case 2:
			createdObject = new vProFighter(-vProFighter::height, randY);
			break;
		default:
			return;
	}

	/* check if bullet, emitted by new object will hit another vEnemy */
	vEnemy	*bulletChecker = NULL;
	for (int i = 0; i < global.winHeight(); ++i) {
		if (!(bulletChecker = dynamic_cast<vEnemy *>(objManager.getObjectByCoords(cIntPair(i, createdObject->gunCoords().y)))))
			continue;
		break;
	}

	/* check if there is enough space on map */
	int		i = -1;
	if (!bulletChecker) {
		for (i = 0; i < createdObject->getSize().y; ++i) {
			if (objManager.getObjectByCoords(cIntPair(0, i))) {
				break;
			}
		}
	}

	if (!bulletChecker && i == createdObject->getSize().y) {
		objManager.addObject(createdObject);
		return;
	}
	delete createdObject;	/* if unable to place on map or bullet may hit vEnemy - delete temp object */
}

void		gameEngine::proceedEnemies() {
	vEnemy	*target;
	for (int currentIndex = 0; currentIndex < objManager.getObjects().size(); ++currentIndex) {
		if (!(target = dynamic_cast<vEnemy *>(objManager.getObjects()[currentIndex])))
			continue;
		if ((clock() + currentIndex) % 50 == 0)			/* currentIndex here only for random purpose */
			shootByEnemy(target);
		if ((clock() + currentIndex) % 15 == 0) {		/* currentIndex here only for random purpose */
			moveEnemy(target);
			if (target->getCoords().x >= global.gameHeight())
				objManager.deleteObject(currentIndex);
		}
	}
	if (clock() % 50 == 0)
		createRandomEnemy();
}

void		gameEngine::proceedBullets() {
	vBullet		*bullet;

	for (int currentIndex = 0; currentIndex < objManager.getObjects().size(); currentIndex++) {
		if (!(bullet = dynamic_cast<vBullet *>(objManager.getObjects()[currentIndex])))
			continue;
		cIntPair	newPosition = cIntPair(bullet->getCoords().x + bullet->direction(), bullet->getCoords().y);
		int			targetIndex = objManager.getIndexByCoords(newPosition);

		if (targetIndex >= 0) {
			vSpaceship	*targetSpaceship;

			if (dynamic_cast<vBullet *>(objManager.getObjects()[targetIndex]))
				objManager.deleteObject(targetIndex);
			else if ((targetSpaceship = dynamic_cast<vSpaceship *>(objManager.getObjects()[targetIndex]))) {
				targetSpaceship->receiveDamage(bullet->damage());
				if (targetSpaceship->hp() == 0) {
					objManager.deleteObject(targetIndex);
					if (!objManager.user()) {
						m_gameStatus = Loose;
						return;
					} else {
						objManager.user()->addLevel();
						if (objManager.user()->level() == 10)
							m_gameStatus = Win;
					}
				}
			}
			objManager.deleteObject(currentIndex);
			continue;
		}

		bullet->move();
		if (bullet->getCoords().x < 0 || bullet->getCoords().x >= global.gameHeight()) {
			objManager.deleteObject(currentIndex);
		}
	}
}

void	gameEngine::shootByUser() {
	vUser		*user = objManager.user();
	objManager.getObjects().push_back(new vBullet(user->gunCoords().x - 1, user->gunCoords().y, user));
}

void	gameEngine::shootByEnemy(vEnemy *shooter) {
	if (!shooter)
		return;
	objManager.getObjects().push_back(new vBullet(shooter->gunCoords().x + 1, shooter->gunCoords().y, shooter));
}

void	gameEngine::manageFrame() {
	static int	seed = 3;

		if (m_frameTimer <= timeManager.timeElapsed()) {
			m_frameTimer += timeManager.frameDelay;
			if (m_gameStatus != Pause)
				seed = (seed == 0) ? 3 : seed - 1;
		}
		objManager.drawFrame(seed);
}

void	gameEngine::manageLoad(int ch) {
	while (m_gameStatus == Startup) {
		if ((ch = getch()) != ERR && ch == ' ')
			m_gameStatus = Pause;
		clear();
		checkWindowSize();
		objManager.drawLogo();
	}
	checkWindowSize();
	objManager.addObject(new vUser(global.gameHeight() - vUser::height - 1, global.winWidth() / 2));
}

void	gameEngine::pause() {
	int ch = 0;

	if ((ch = getch()) == ' ' || ch == 'p')
        m_gameStatus = Running;
	else if (ch == 'q')
		m_gameStatus = Exiting;
	checkWindowSize();
	clear();
	objManager.showAllObjects();
	manageFrame();
	manageMenu();
	objManager.clean();
	objManager.drawPause();
}

void gameEngine::checkWindowSize() {
	int h = global.winHeight();
	int w = global.winHeight();

	global.updateWinSize();
	if (h != global.winHeight() && w != global.winWidth() && m_gameStatus != Startup)
		exitGame(1);
	while (1) {
		if ((global.winWidth() < 130 && global.winWidth() > 70) && (global.winHeight() < 74 && global.winHeight() > 60))
			return;
		refresh();
		clear();
		objManager.drawWindowSize();
		global.updateWinSize();
	}
}

void	gameEngine::exitGame(int fl) {
	clear();
	endwin();

	switch (fl) {
		case 1:
			std::cout << "You can't resize window, when you are playing !\n";
			exit(1);
		default:
			break;
	}
}

void   gameEngine::manageWinWindow() {
    int ch;

    objManager.drawWinWindow();
    while (true) {
        if ((ch = getch()) == ' ') {
            m_gameStatus = Running;
            break;
        } else if (ch == 'q')
			break ;
        objManager.drawWinWindow();
    }
}

void gameEngine::manageLooseWindow() {
    int ch;

    while (true) {
        ch = getch();
        if (ch == ' ') {
            m_gameStatus = Running;
            break;
        } else if (ch == 'q')
            break ;
        objManager.drawLoseWindow();
    }
}

void gameEngine::addGameTime() {
    m_gameTime -= 0.05;
}

void gameEngine::manageMenu() {
	objManager.drawMenu((int)m_gameTime);
}

GameStatus	gameEngine::status() const {
	return (m_gameStatus);
}

void gameEngine::setStatus(GameStatus status) {
	m_gameStatus = status;
}

void gameEngine::newGame() {
	objManager.getObjects().clear();
	m_gameTime = 120;
	timeManager.resetStart();
	objManager.addObject(new vUser(global.gameHeight() - vUser::height - 1, global.winWidth() / 2));
}

int gameEngine::getGameTime() {
	return ((int)m_gameTime);
}