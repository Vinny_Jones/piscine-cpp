//
// 42 header
//

#include "vObject.hpp"

vObject::vObject() {}		/* private constructor */

vObject::vObject(int x, int y, int height, int width) {
	setSize(height, width);
	setCoords(x, y);
}

vObject::vObject(const vObject &copy) {
	*this = copy;
}

vObject::~vObject() {}

vObject		&vObject::operator=(const vObject &assign) {
	m_shape = assign.m_shape;
	m_coords = assign.m_coords;
	m_size = assign.m_size;
	return (*this);
}

const cIntPair	&vObject::getCoords() const {
	return (m_coords);
}

void	vObject::setCoords(int x, int y) {
	m_coords.x = x;
	m_coords.y = y;
}

const cIntPair	&vObject::getSize() const {
	return (m_size);
}

void	vObject::setSize(int height, int width) {
	m_size.x = height;
	m_size.y = width;
}

const cList<std::wstring>	&vObject::getShape() {
	return (m_shape);
}
