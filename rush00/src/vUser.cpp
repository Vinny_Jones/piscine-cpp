//
// 42 header
//

#include "vUser.hpp"

const int	vUser::width = 5;
const int	vUser::height = 4;

vUser::vUser() : vSpaceship(0, 0, 0, 0) {}		/* private constructor */

vUser::vUser(int x, int y)
		: vSpaceship(x, y, vUser::height, vUser::width)
{
	setShape();
	m_hp = 40;
	m_maxHP = 40;
	m_damage = 10;
	m_armor = 1;
	m_gunCoords = cIntPair(0, 2);
}

vUser::vUser(const vUser &copy) : vSpaceship(copy) {
	*this = copy;
}

vUser::~vUser() {}

vUser	&vUser::operator=(const vUser &assign) {
	vSpaceship::operator=(assign);
	return (*this);
}

void	vUser::setShape() {
	m_shape.clear();
	m_shape.push_back(L"  ⏅ ");
	m_shape.push_back(L"⎛ ☠ ⎞");
	m_shape.push_back(L"⎣ ☢ ⎦");
	m_shape.push_back(L"  ˇˇ ");
}
