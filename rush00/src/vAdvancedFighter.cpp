#include "vAdvancedFighter.hpp"

int	vAdvancedFighter::width = 4;
int	vAdvancedFighter::height = 3;

vAdvancedFighter::vAdvancedFighter() : vEnemy() {}		/* private constructor */

vAdvancedFighter::vAdvancedFighter(int x, int y)
        : vEnemy(x, y, vAdvancedFighter::height, vAdvancedFighter::width)
{
    setShape();
    m_hp = 15;
    m_maxHP = 15;
    m_damage = 6;
    m_armor = 2;
    m_gunCoords = cIntPair(2, 1);
}

vAdvancedFighter::vAdvancedFighter(const vAdvancedFighter &copy) : vEnemy(copy) {
    *this = copy;
}

vAdvancedFighter::~vAdvancedFighter() {}

vAdvancedFighter	&vAdvancedFighter::operator=(const vAdvancedFighter &assign) {
    vEnemy::operator=(assign);
    return (*this);
}

void	vAdvancedFighter::setShape() {
    m_shape.clear();
    m_shape.push_back(L"{--}");
    m_shape.push_back(L"\\  /");
    m_shape.push_back(L" \\/ ");
}