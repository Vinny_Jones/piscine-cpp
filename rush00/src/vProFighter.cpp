#include "vProFighter.hpp"

int	vProFighter::width = 5;
int	vProFighter::height = 4;

vProFighter::vProFighter() : vEnemy() {}		/* private constructor */

vProFighter::vProFighter(int x, int y)
        : vEnemy(x, y, vProFighter::height, vProFighter::width)
{
    setShape();
    m_hp = 20;
    m_maxHP = 20;
    m_damage = 8;
    m_armor = 3;
    m_gunCoords = cIntPair(3, 2);
}

vProFighter::vProFighter(const vProFighter &copy) : vEnemy(copy) {
    *this = copy;
}

vProFighter::~vProFighter() {}

vProFighter	&vProFighter::operator=(const vProFighter &assign) {
    vEnemy::operator=(assign);
    return (*this);
}

void	vProFighter::setShape() {
    m_shape.clear();
    m_shape.push_back(L"/---\\");
    m_shape.push_back(L"\\   /");
    m_shape.push_back(L" \\ / ");
    m_shape.push_back(L"  ˇ  ");
}