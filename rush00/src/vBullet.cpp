//
// 42 header
//

#include <curses.h>
#include "vBullet.hpp"

int	vBullet::width = 1;
int	vBullet::height = 1;

vBullet::vBullet() : vObject(0, 0, 0, 0) {}		/* private constructor */

vBullet::vBullet(int x, int y, vSpaceship *emitter)
		: vObject(x, y, vBullet::height, vBullet::width)
{
	setShape();
	m_direction = (dynamic_cast<vUser *>(emitter) ? -1 : 1);
	m_damage = emitter->damage();
}

vBullet::vBullet(const vBullet &copy) : vObject(copy) {
	*this = copy;
}

vBullet::~vBullet() {}

vBullet		&vBullet::operator=(const vBullet &assign) {
	vObject::operator=(assign);
	m_direction = assign.m_direction;
	return (*this);
}

int			vBullet::damage() const {
	return (m_damage);
}

int		vBullet::direction() const {
	return (m_direction);
}

void	vBullet::move() {
	setCoords(m_coords.x + m_direction, m_coords.y);
}

void	vBullet::setShape() {
	m_shape.clear();
	m_shape.push_back(L"•");
}
