/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objectManager.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 18:05:11 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 18:05:16 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objectManager.hpp"

objectManager::objectManager(globalInfo *gi) : m_global(gi) {}

objectManager::~objectManager() {
	for (int i = 0; i < m_objects.size(); ++i) {
		if (m_objects[i]){
			delete m_objects[i];
		}
	}
}

vUser	*objectManager::user() const {
	return (dynamic_cast<vUser *>(m_objects.first()));
}

cList<vObject *>	&objectManager::getObjects() {
	return (m_objects);
}

void	objectManager::showAllObjects() const {
	vObject		*current;
	for (int i = 0; i < m_objects.size(); i++) {
		if (!(current = m_objects[i]))
			continue;
		for (int j = 0; j < current->getSize().x; ++j) {
			mvaddwstr(current->getCoords().x + j, current->getCoords().y, current->getShape()[j].c_str());
		}
	}
}

bool	objectManager::addObject(vObject *target) {
	if (!target)
		return (false);
	for (int i = 0; i < m_objects.size(); ++i) {
		if (m_objects[i] == target)
			return (false);
	}
	m_objects.push_back(target);
	return (true);
}

vObject	*objectManager::getObjectByCoords(const cIntPair &coords) {
	int index = getIndexByCoords(coords);
	return (index >= 0 ? m_objects[index] : NULL);
}

int		objectManager::getIndexByCoords(const cIntPair &coords) {
	if (coords.x < 0 || coords.x >= m_global->winHeight()
		|| coords.y < 0 || coords.y >= m_global->winWidth())
	{
		return (-1);
	}

	vObject		*current;
	for (int i = 0; i < m_objects.size(); ++i) {
		if (!(current = m_objects[i]))
			continue;
		cIntPair	cCoords = current->getCoords();
		cIntPair	cSize = current->getSize();

		if (cCoords.x <= coords.x && cCoords.x + cSize.x > coords.x
			&& cCoords.y <= coords.y && cCoords.y + cSize.y > coords.y)
		{
			return (i);
		}
	}
	return (-1);
}

void	objectManager::deleteObject(int index) {
	delete m_objects[index];
	m_objects[index] = NULL;
}

void	objectManager::deleteObject(vObject *target) {
	if (!target)
		return;
	for (int i = 0; i < m_objects.size(); ++i) {
		if (m_objects[i] == target) {
			return (deleteObject(i));
		}
	}
}

void objectManager::drawFrame(int index) const {
	std::wstring	lside[4] = {L"༼", L"{", L"༽", L"}"};
	std::wstring	rside[4] = {L"༽", L"}", L"༼", L"{"};

	for (int i = 0; i < m_global->gameHeight(); i++) {
		mvaddwstr(i, 0, lside[(index + i) % 4].c_str());
		mvaddwstr(i, m_global->winWidth() - 1, rside[(index + i) % 4].c_str());
	}
	for (int i = m_global->gameHeight(); i < m_global->winHeight() - 1; i++) {
		mvaddch(i, 0, '|');
		mvaddch(i, m_global->winWidth() - 1, '|');
	}
	for (int j = 0; j < m_global->winWidth(); ++j) {
		mvaddch(m_global->gameHeight() - 1, j, '~');
		mvaddch(m_global->winHeight() - 1, j, '-');
		mvaddch(m_global->gameHeight(), j, '-');
	}
}

void objectManager::drawMenu(int gameTime) {
	std::stringstream 	convert;
	int 				sixWidth = m_global->winWidth() / 6;
    int 				quarterWidth = m_global->winWidth() / 4;

    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_BLUE, COLOR_BLACK);
    init_pair(3, COLOR_GREEN, COLOR_BLACK);
    init_pair(4, COLOR_YELLOW, COLOR_BLACK);
    attron(COLOR_PAIR(3));
    convert << "HP: " + std::to_string(user()->hp());
	mvaddstr(m_global->gameHeight() + 1, sixWidth - 3, convert.str().c_str());
	convert.str(std::string());
	convert.clear();
    attron(COLOR_PAIR(2));
	convert << "Armor: " + std::to_string(user()->armor());
	mvaddstr(m_global->gameHeight() + 1, sixWidth * 2 - 4, convert.str().c_str());
	convert.str(std::string());
	convert.clear();
    attron(COLOR_PAIR(1));
	convert << "Damage: " + std::to_string(user()->damage());;
	mvaddstr(m_global->gameHeight() + 1, sixWidth * 3 - 4, convert.str().c_str());
	convert.str(std::string());
	convert.clear();
    attron(COLOR_PAIR(4));
	convert << "Score: " + std::to_string(user()->level());
	mvaddstr(m_global->gameHeight() + 1, sixWidth * 4 - 4, convert.str().c_str());
	convert.str(std::string());
	convert.clear();
	attroff(COLOR_PAIR(4));
	convert << "Time: " + std::to_string(gameTime);
	mvaddstr(m_global->gameHeight() + 1, sixWidth * 5 - 4, convert.str().c_str());
	convert.str(std::string());
	convert.clear();
    mvaddstr(m_global->gameHeight() + 3, quarterWidth - 6, "space - fire");
    mvaddstr(m_global->gameHeight() + 3, quarterWidth * 2 - 4, "p - pause");
    mvaddstr(m_global->gameHeight() + 3, quarterWidth * 3 - 4, "q - quit");
}

void    objectManager::drawLogo() {
    mvaddstr(m_global->winHeight() / 2 - 5, m_global->winWidth() / 2 - 24,
            " ______    _______  _______  ______    _______ ");
    mvaddstr(m_global->winHeight() / 2 - 4, m_global->winWidth() / 2 - 24,
            "|    _ |  |       ||       ||    _ |  |       |");
    mvaddstr(m_global->winHeight() / 2 - 3, m_global->winWidth() / 2 - 24,
            "|   | ||  |    ___||_     _||   | ||  |   _   |");
    mvaddstr(m_global->winHeight() / 2 - 2, m_global->winWidth() / 2 - 24,
            "|   |_||_ |   |___   |   |  |   |_||_ |  | |  |");
    mvaddstr(m_global->winHeight() / 2 - 1, m_global->winWidth() / 2 - 24,
            "|    __  ||    ___|  |   |  |    __  ||  |_|  |");
    mvaddstr(m_global->winHeight() / 2, m_global->winWidth() / 2 - 24,
            "|   |  | ||   |___   |   |  |   |  | ||       |");
    mvaddstr(m_global->winHeight() / 2 + 1, m_global->winWidth() / 2 - 24,
            "|___|  |_||_______|  |___|  |___|  |_||_______|");
    mvaddstr(m_global->winHeight() / 2 + 3, m_global->winWidth() / 2 - 13,
             "press space to start game");
}

void    objectManager::drawPause() {
    mvaddstr(m_global->winHeight() / 2 - 5, m_global->winWidth() / 2 - 15,
			 "  _____                     ");
    mvaddstr(m_global->winHeight() / 2 - 4, m_global->winWidth() / 2 - 15,
             " |  __ \\                    ");
    mvaddstr(m_global->winHeight() / 2 - 3, m_global->winWidth() / 2 - 15,
             " | |__) |_ _ _   _ ___  ___ ");
    mvaddstr(m_global->winHeight() / 2 - 2, m_global->winWidth() / 2 - 15,
             " |  ___/ _` | | | / __|/ _ \\");
    mvaddstr(m_global->winHeight() / 2 - 1, m_global->winWidth() / 2 - 15,
             " | |  | (_| | |_| \\__ \\  __/");
    mvaddstr(m_global->winHeight() / 2, m_global->winWidth() / 2 - 15,
             " |_|   \\__,_|\\__,_|___/\\___|");
    mvaddstr(m_global->winHeight() / 2 + 1, m_global->winWidth() / 2 - 15,
             "                            ");
    mvaddstr(m_global->winHeight() / 2 + 3, m_global->winWidth() / 2 - 12,
             "press space to continue");
}

void objectManager::drawWindowSize() {
	mvaddstr(m_global->winHeight() / 2, m_global->winWidth() / 2 - 14,
			 "Big or small size of window.");
	mvaddstr(m_global->winHeight() / 2 + 2, m_global->winWidth() / 2 - 9,
			 "Please, changed it.");
}

void	objectManager::clean() {
    for (int i = m_objects.size() - 1; i >= 0; i--) {
        if (m_objects[i] == NULL) {
            m_objects.remove(i);
        }
    }
}

void objectManager::drawWinWindow() {
    mvaddstr(m_global->winHeight() / 2 - 5, m_global->winWidth() / 2 - 20,
             "__   __            _    _ _       \n");
    mvaddstr(m_global->winHeight() / 2 - 4, m_global->winWidth() / 2 - 20,
             "\\ \\ / /           | |  | (_)      \n");
    mvaddstr(m_global->winHeight() / 2 - 3, m_global->winWidth() / 2 - 20,
             " \\ V /___  _   _  | |  | |_ _ __  \n");
    mvaddstr(m_global->winHeight() / 2 - 2, m_global->winWidth() / 2 - 20,
             "  \\ // _ \\| | | | | |/\\| | | '_ \\ \n");
    mvaddstr(m_global->winHeight() / 2 - 1, m_global->winWidth() / 2 - 20,
             "  | | (_) | |_| | \\  /\\  / | | | |\n");
    mvaddstr(m_global->winHeight() / 2, m_global->winWidth() / 2 - 20,
             "  \\_/\\___/ \\__,_|  \\/  \\/|_|_| |_|\n");
    mvaddstr(m_global->winHeight() / 2 + 1, m_global->winWidth() / 2 - 20,
             "                                  \n");
    mvaddstr(m_global->winHeight() / 2 + 3, m_global->winWidth() / 2 - 23,
             "press space to start game again and q - to quit");
}

void objectManager::drawLoseWindow() {
    mvaddstr(m_global->winHeight() / 2 - 5, m_global->winWidth() / 2 - 20,
             "                    __                \n");
    mvaddstr(m_global->winHeight() / 2 - 4, m_global->winWidth() / 2 - 20,
             "/\\_/\\___  _   _    / /  ___  ___  ___ \n");
    mvaddstr(m_global->winHeight() / 2 - 3, m_global->winWidth() / 2 - 20,
             "\\_ _/ _ \\| | | |  / /  / _ \\/ __|/ _ \\\n");
    mvaddstr(m_global->winHeight() / 2 - 2, m_global->winWidth() / 2 - 20,
             " / \\ (_) | |_| | / /__| (_) \\__ \\  __/\n");
    mvaddstr(m_global->winHeight() / 2 - 1, m_global->winWidth() / 2 - 20,
             " \\_/\\___/ \\__,_| \\____/\\___/|___/\\___|\n");
    mvaddstr(m_global->winHeight() / 2 + 1, m_global->winWidth() / 2 - 23,
             "press space to start game again and q - to quit");
}