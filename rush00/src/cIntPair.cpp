/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cIntPair.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 18:04:43 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 18:04:44 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cIntPair.hpp"

intPair::intPair(int setX, int setY) : x(setX), y(setY) {}

intPair::intPair(const intPair &copy) : x(copy.x), y(copy.y) {}

intPair::~intPair() {}

intPair& intPair::operator=(const intPair &assign) {
	x = assign.x;
	y = assign.y;
	return (*this);
}

std::ostream	&operator<<(std::ostream &os, const intPair &obj) {
	os << obj.x << " " << obj.y;
	return (os);
}
