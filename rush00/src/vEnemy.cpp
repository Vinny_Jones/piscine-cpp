//
// 42 header
//

#include "vEnemy.hpp"

vEnemy::vEnemy() : vSpaceship(0, 0, 0, 0) {}		/* private constructor */

vEnemy::vEnemy(int x, int y, int h, int w)
		: vSpaceship(x, y, h, w) {}

vEnemy::vEnemy(const vEnemy &copy) : vSpaceship(copy) {
	*this = copy;
}

vEnemy::~vEnemy() {}

vEnemy	&vEnemy::operator=(const vEnemy &assign) {
	vSpaceship::operator=(assign);
	return (*this);
}
