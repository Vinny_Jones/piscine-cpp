/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 17:34:06 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 17:34:09 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gameEngine.hpp"

void	runProgram();

int 	main() {
	runProgram();

	return (0);
}

void	runProgram() {
	setlocale(LC_ALL, "");
	initscr();
	noecho();
	curs_set(FALSE);
	nodelay(stdscr, TRUE);
	keypad(stdscr, TRUE);
	scrollok(stdscr, FALSE);
	cbreak();

	gameEngine		game;

	int 	ch;
	while (game.status() == Startup || game.status() == Running || game.status() == Pause) {
		if (game.status() == Pause) {
			game.pause();
			continue;
		}

		if ((ch = getch()) != ERR)
			game.handleKey(ch);

		if (game.status() == Startup)
			game.manageLoad(ch);

		game.checkWindowSize();

		/* proceed game entries - moves, shoots etc. */
		game.proceedBullets();

		if (game.getGameTime() <= 0)
			game.setStatus(Loose);
		if (game.status() == Loose || game.status() == Win) {
			game.status() == Win ? game.manageWinWindow() : game.manageLooseWindow();
            if (game.status() == Running) {
				game.newGame();
				continue;
            }
            break;
        }
		game.proceedEnemies();

		/* show visual elements */
		game.objManager.showAllObjects();
		game.manageFrame();
		game.manageMenu();
		game.objManager.clean();

		refresh();
		game.timeManager.wait(0.05);
        game.addGameTime();
		clear();
	}
	/* ToDo: memory leak fix on couple games */

	endwin();
}
