//
// 42 header
//

#include "vSpaceship.hpp"

vSpaceship::vSpaceship() : vObject(0, 0, 0, 0) {}		/* private constructor */

vSpaceship::vSpaceship(int x, int y, int height, int width)
		: vObject(x, y, height, width), m_hp(0), m_maxHP(0), m_damage(0), m_level(0)
{}

vSpaceship::vSpaceship(const vSpaceship &copy) : vObject(copy) {
	*this = copy;
}

vSpaceship::~vSpaceship() {}

vSpaceship	&vSpaceship::operator=(const vSpaceship &assign) {
	vObject::operator=(assign);
	m_gunCoords = assign.m_gunCoords;
	m_hp = assign.m_hp;
	m_maxHP = assign.m_maxHP;
	m_damage = assign.m_damage;
	m_armor = assign.m_armor;
	m_level = assign.m_level;
	return (*this);
}

cIntPair	vSpaceship::gunCoords() const {
	cIntPair	coords;

	coords.x = getCoords().x + m_gunCoords.x;
	coords.y = getCoords().y + m_gunCoords.y;
	return (coords);
}

void		vSpaceship::receiveDamage(int damage) {
	if (damage <= 0)
		return;
	m_hp = (m_hp > (damage - m_armor)) ? m_hp - damage + m_armor : 0;
}

int			vSpaceship::damage() const {
	return (m_damage);
}

int			vSpaceship::hp() const {
	return (m_hp);
}

int			vSpaceship::armor() const {
	return (m_armor);
}

int			vSpaceship::level() const {
	return (m_level);
}

void vSpaceship::addLevel() {
	m_level++;
}