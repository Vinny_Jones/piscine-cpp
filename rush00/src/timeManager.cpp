/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   timeManager.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 18:05:25 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 18:05:30 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "timeManager.hpp"

const double timeManager::frameDelay = 0.4;

timeManager::timeManager() {
	resetStart();
}

timeManager::~timeManager() {}

timeManager::timeManager(const timeManager &) {}			/* private copy constructor */

timeManager &timeManager::operator=(const timeManager &) {	/* private assignment operator */
	return (*this);
}

void	timeManager::resetStart() {
	m_startTime = clock();
}

double	timeManager::timeElapsed() const {
	return ((double)(clock() - m_startTime) / CLOCKS_PER_SEC);
}

void timeManager::wait(double seconds) {
	clock_t	start = clock();
	while (((double)clock() - start) / CLOCKS_PER_SEC < seconds)
		;
}
