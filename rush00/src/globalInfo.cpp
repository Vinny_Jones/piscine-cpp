/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globalInfo.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 18:05:58 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 18:06:04 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globalInfo.hpp"

globalInfo::globalInfo() {
	updateWinSize();
}

globalInfo::~globalInfo() {
}

void	globalInfo::updateWinSize() {
	m_winSize.x = LINES;
	m_winSize.y = COLS;
	game_Height = LINES - LINES / 12;
}

int		globalInfo::winHeight() const {
	return (m_winSize.x);
}

int		globalInfo::winWidth() const {
	return (m_winSize.y);
}

int		globalInfo::gameHeight() const {
	return (game_Height);
}
