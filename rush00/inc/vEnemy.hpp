//
// 42 header
//

#ifndef VENEMY_HPP
# define VENEMY_HPP

#include "vSpaceship.hpp"

class vEnemy : public vSpaceship {
public:
	vEnemy(int x, int y, int h, int w);
	vEnemy(const vEnemy &copy);
	~vEnemy();
	vEnemy	&operator=(const vEnemy &assign);

protected:
	vEnemy();

private:
	virtual void	setShape() = 0;
};

#endif