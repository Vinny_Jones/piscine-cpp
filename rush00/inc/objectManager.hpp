/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objectManager.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:46:41 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 20:46:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OBJECTMANAGER_HPP
# define OBJECTMANAGER_HPP

# include "globalInfo.hpp"
# include "vUser.hpp"
# include "vEnemy.hpp"
# include "vBullet.hpp"
# include "vBasicFighter.hpp"
# include "vAdvancedFighter.hpp"
# include "vProFighter.hpp"
# include "timeManager.hpp"

class objectManager {
public:
	explicit objectManager(globalInfo *gi);
	virtual ~objectManager();

	vUser				*user() const;
	cList<vObject *>	&getObjects();
	void				showAllObjects() const;
	vObject				*getObjectByCoords(const cIntPair &coords);
	int 				getIndexByCoords(const cIntPair &coords);
	bool				addObject(vObject *target);
	void 				deleteObject(int index);
	void 				deleteObject(vObject *target);
	void 				drawFrame(int index) const;
	void				drawMenu(int gameTime);
	void				drawLogo();
	void				drawPause();
	void				drawWindowSize();
    void                drawWinWindow();
    void                drawLoseWindow();
	void 				clean();

private:
	objectManager();
	objectManager(const objectManager &copy);
	objectManager	&operator=(const objectManager &assign);

	globalInfo			*m_global;
	cList<vObject *>	m_objects;
};


#endif
