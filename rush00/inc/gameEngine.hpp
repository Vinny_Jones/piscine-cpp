/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gameEngine.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:46:30 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 20:46:34 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAMEENGINE_HPP
# define GAMEENGINE_HPP

# include <locale>
# include <curses.h>
# include "timeManager.hpp"
# include "globalInfo.hpp"
# include "objectManager.hpp"

enum GameStatus {
	Startup,
	Running,
	Pause,
	Win,
	Loose,
	Exiting
};

class gameEngine {
public:
	gameEngine();
	virtual ~gameEngine();

	globalInfo		global;
	objectManager	objManager;
	timeManager		timeManager;

	bool			moveUser(int toX, int toY);
	void			handleKey(int ch);
	void			proceedBullets();
	void 			proceedEnemies();
	void 			createRandomEnemy();
	bool			moveEnemy(vEnemy *target);
	void			shootByUser();
	void			shootByEnemy(vEnemy *shooter);
	GameStatus		status() const;
    void            setStatus(GameStatus status);
	void 			manageFrame();
	void 			manageMenu();
	void 			manageLoad(int ch);
	void			pause();
	void			checkWindowSize();
	void			exitGame(int fl);
    void            addGameTime();
    void            manageWinWindow();
    void            manageLooseWindow();
    void            newGame();
    int             getGameTime();

private:
	gameEngine(const gameEngine &copy);
	gameEngine	&operator=(const gameEngine &assign);
	GameStatus	m_gameStatus;
	double 		m_frameTimer;
    double      m_gameTime;
};

#endif
