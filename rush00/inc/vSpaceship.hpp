/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vSpaceship.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:47:24 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 20:47:26 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VSPACESHIP_HPP
# define VSPACESHIP_HPP

# include "vObject.hpp"
# include <curses.h>
# include <string>

class vSpaceship : public vObject {
public:
	vSpaceship(int x, int y, int width, int height);
	vSpaceship(const vSpaceship &copy);
	virtual ~vSpaceship();
	vSpaceship	&operator=(const vSpaceship &assign);

	cIntPair	gunCoords() const;
	void 		receiveDamage(int damage);
	int 		damage() const;
	int			hp() const;
	int			armor() const;
	int			level() const;
	void		addLevel();
protected:
	cIntPair	m_gunCoords;
	int 		m_hp;
	int			m_maxHP;
	int			m_damage;
	int 		m_armor;
	int 		m_level;
private:
	vSpaceship();
};

#endif
