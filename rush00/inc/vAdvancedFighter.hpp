#ifndef vAdvancedFighter_HPP
# define vAdvancedFighter_HPP

# include "vEnemy.hpp"

class vAdvancedFighter : public vEnemy{

public:
    static int	width;
    static int	height;
    vAdvancedFighter(int x, int y);
    vAdvancedFighter(const vAdvancedFighter &copy);
    ~vAdvancedFighter();
    vAdvancedFighter	&operator=(const vAdvancedFighter &assign);

private:
    void	setShape();
    vAdvancedFighter();
};

#endif