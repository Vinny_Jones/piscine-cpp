#ifndef vProFighter_HPP
# define vProFighter_HPP

# include "vEnemy.hpp"

class vProFighter : public vEnemy{
public:
    static int	width;
    static int	height;
    vProFighter(int x, int y);
    vProFighter(const vProFighter &copy);
    ~vProFighter();
    vProFighter	&operator=(const vProFighter &assign);

private:
    void	setShape();
    vProFighter();
};

#endif