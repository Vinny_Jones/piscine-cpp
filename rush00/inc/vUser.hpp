//
// 42 header
//

#ifndef VUSER_HPP
# define VUSER_HPP

#include "vSpaceship.hpp"

#include <string>

class vUser : public vSpaceship {
public:
	const static int	width;
	const static int	height;
	vUser(int x, int y);
	vUser(const vUser &copy);
	~vUser();
	vUser	&operator=(const vUser &assign);

private:
	void	setShape();
	vUser();
};

#endif
