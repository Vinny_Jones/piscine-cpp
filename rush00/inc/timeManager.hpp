/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   timeManager.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:46:59 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 20:47:01 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TIMEMANAGER_HPP
# define TIMEMANAGER_HPP

#include <time.h>

class timeManager {
public:
	const static double	frameDelay;
	timeManager();
	virtual ~timeManager();

	void	resetStart();
	double	timeElapsed() const;
	void	wait(double seconds);

private:
	clock_t		m_startTime;
	timeManager(const timeManager &);
	timeManager	&operator=(const timeManager &);
};

#endif
