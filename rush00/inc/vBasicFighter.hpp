#ifndef VBASICFIGHTER_HPP
# define VBASICFIGHTER_HPP

# include "vEnemy.hpp"

class vBasicFighter : public vEnemy{

public:
    static int	width;
    static int	height;
    vBasicFighter(int x, int y);
    vBasicFighter(const vBasicFighter &copy);
    ~vBasicFighter();
    vBasicFighter	&operator=(const vBasicFighter &assign);

private:
    void	setShape();
    vBasicFighter();
};

#endif