//
// 42 header
//

#ifndef CINTPAIR_HPP
# define CINTPAIR_HPP

# include <iostream>

typedef struct intPair {
	intPair(int setX = 0, int setY = 0);
	intPair(const intPair &copy);
	virtual ~intPair();
	intPair		&operator=(const intPair &assign);

	int 	x;
	int 	y;
}				cIntPair;

std::ostream	&operator<<(std::ostream &os, const intPair &obj);

#endif