/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vObject.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:48:25 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 20:48:30 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VOBJECT_HPP
# define VOBJECT_HPP

# include "cList.hpp"
# include "cIntPair.hpp"
# include <string>

class vObject {
public:
	vObject(int x, int y, int height, int width);
	vObject(const vObject &copy);
	virtual ~vObject();
	vObject &operator=(const vObject &assign);

	const cIntPair				&getCoords() const;
	void						setCoords(int x, int y);
	const cIntPair				&getSize() const;
	void						setSize(int x, int y);
	const cList<std::wstring>	&getShape();
protected:
	virtual void 				setShape() = 0;
	cList<std::wstring>	m_shape;
	cIntPair			m_coords;
	cIntPair			m_size;
private:
	vObject();
};

#endif
