/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vBullet.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:48:10 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 20:48:13 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VBULLET_HPP
# define VBULLET_HPP

# include "vUser.hpp"

class vBullet : public vObject {
public:
	static int	width;
	static int	height;

	vBullet(int x, int y, vSpaceship *emitter);
	vBullet(const vBullet &copy);
	virtual ~vBullet();
	vBullet	&operator=(const vBullet &assign);

	int 		damage() const;
	int			direction() const;
	void		move();

private:
	vBullet();
	void		setShape();
	int 		m_damage;
	int 		m_direction;
};

#endif
