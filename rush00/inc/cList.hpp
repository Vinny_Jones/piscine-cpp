//
// 42 header
//

#ifndef LIST_HPP
# define LIST_HPP

#include <iostream>
#include <sstream>

template <typename T>
class cList {
public:
	cList();
	cList(const cList &copy);
	virtual	~cList();
	inline int	size() const;
	inline bool	empty() const;
	void		push_front(const T &element);
	void		push_back(const T &element);
	T			&first() const;
	T			&last() const;
	void		clear();
	void		remove(int i);
	inline void	remove_first();
	inline void	remove_last();
	T			&operator[](int i) const;
	cList<T>	&operator=(const cList &assign);
private:
	struct listElement {
		explicit listElement(const T &obj) : element(obj), next(NULL), prev(NULL) {};
		T			element;
		listElement	*next;
		listElement	*prev;
	private:
		listElement();
	};

	listElement	*m_listFirst;
	listElement	*m_listLast;
	int			m_size;
};

/* Fast inserters overloads. Really cool stuff */
template <typename T>
cList<T>		&operator<<(cList<T> &list, const T &element) {
	list.push_back(element);
	return (list);
}

template <typename T>
cList<T>		&operator>>(cList<T> &list, const T &element) {
	list.push_front(element);
	return (list);
}
/* Fast inserters overloads. Really cool stuff */

template <typename T>
cList<T>::cList() {
	m_size = 0;
	m_listFirst = m_listLast = NULL;
}

template <typename T>
cList<T>::cList(const cList &copy) {
	m_size = 0;
	m_listFirst = m_listLast = NULL;
	*this = copy;
}

template <typename T>
cList<T>::~cList() {
	clear();
}

template <typename T>
int		cList<T>::size() const {
	return (m_size);
}

template <typename T>
bool	cList<T>::empty() const {
	return (size() == 0);
}

template <typename T>
void	cList<T>::push_front(const T &element) {
	listElement *tempElem = new listElement(element);
	if (m_listFirst) {
		tempElem->next = m_listFirst;
		m_listFirst->prev = tempElem;
		m_listFirst = tempElem;
	} else
		m_listFirst = m_listLast = tempElem;
	m_size++;
}

template <typename T>
void	cList<T>::push_back(const T &element) {
	listElement *tempElem = new listElement(element);
	if (m_listLast) {
		tempElem->prev = m_listLast;
		m_listLast->next = tempElem;
		m_listLast = tempElem;
	} else
		m_listFirst = m_listLast = tempElem;
	m_size++;
}

template <typename T>
T		&cList<T>::first() const {
	if (empty())
		throw std::logic_error("Can't access elements of empty container");
	return (m_listFirst->element);
}

template <typename T>
T		&cList<T>::last() const {
	if (empty())
		throw std::logic_error("Can't access elements of empty container");
	return (m_listLast->element);
}

template <typename T>
void	cList<T>::clear() {
	listElement *current;
	while (m_listFirst) {
		current = m_listFirst;
		m_listFirst = m_listFirst->next;
		delete current;
	}
	m_size = 0;
	m_listFirst = m_listLast = NULL;
}

template <typename T>
void	cList<T>::remove(int i) {
	if (i < 0 || i >= m_size) {
		throw std::logic_error("[remove]List index out of range");
	}
	listElement	*current = m_listFirst;
	while (i--)
		current = current->next;
	if (current == m_listFirst)
		m_listFirst = m_listFirst->next;
	else
		current->prev->next = current->next;
	if (current == m_listLast)
		m_listLast = m_listLast->prev;
	else
		current->next->prev = current->prev;
	delete current;
	m_size--;
}

template <typename T>
void	cList<T>::remove_first() {
	if (m_size)
		cList<T>::remove(0);
}

template <typename T>
void	cList<T>::remove_last() {
	if (m_size)
		cList<T>::remove(m_size - 1);
}

template <typename T>
T		&cList<T>::operator[](int i) const {
	if (i < 0 || i >= m_size) {
		std::stringstream	error;
		error << "[get access]List index out of range: size is " << m_size << ", index is " << i << std::endl;
		throw std::logic_error(error.str());
	}
	listElement	*current = m_listFirst;
	while (i--)
		current = current->next;
	return (current->element);
}

template <typename T>
cList<T>	&cList<T>::operator=(const cList &assign) {
	/* if assign == *this */
	clear();
	for (int i = 0; i < assign.size(); i++) {
		*this << assign[i];
	}
	return (*this);
}

#endif
