/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globalInfo.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:47:14 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/21 20:47:15 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GLOBALINFO_HPP
# define GLOBALINFO_HPP

# include <curses.h>
# include <iostream>
# include <utility>
# include "cList.hpp"
# include "cIntPair.hpp"
# include "vUser.hpp"

class globalInfo {
public:
	globalInfo();
	virtual	~globalInfo();

	void 	updateWinSize();
	int 	winWidth() const;
	int 	winHeight() const;
	int 	gameHeight() const;
private:
	globalInfo(const globalInfo &copy);
	globalInfo	&operator=(const globalInfo &assign);

	cIntPair	m_winSize;
	int			game_Height;
};

#endif
