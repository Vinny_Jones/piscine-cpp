/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:10:42 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:10:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PresidentialPardonForm.hpp"

const int PresidentialPardonForm::requiredSignGrade = 25;
const int PresidentialPardonForm::requiredExecGrade = 5;

PresidentialPardonForm::PresidentialPardonForm(const std::string &target)
	: Form(target, PresidentialPardonForm::requiredSignGrade, PresidentialPardonForm::requiredExecGrade)
{}


PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm &copy) : Form(copy) {}

PresidentialPardonForm::~PresidentialPardonForm() {}

PresidentialPardonForm	&PresidentialPardonForm::operator=(const PresidentialPardonForm &assign) {
	Form::operator=(assign);
	return (*this);
}

void	PresidentialPardonForm::action() const {
	std::cout << getName() << " has been pardoned by Zafod Beeblebrox." << std::endl;
}

std::ostream	&operator<<(std::ostream &os, PresidentialPardonForm &object) {
	Form	*base = dynamic_cast<Form *>(&object);
	os << "PresidentialPardon " << *base;
	return (os);
}
