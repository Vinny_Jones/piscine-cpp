/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.hpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:10:59 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:11:00 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP

# include "Form.hpp"
# include <ctime>

class RobotomyRequestForm : public Form {
public:
	static const int requiredSignGrade;
	static const int requiredExecGrade;

	RobotomyRequestForm(const std::string &target);
	RobotomyRequestForm(const RobotomyRequestForm &copy);
	virtual ~RobotomyRequestForm();
	RobotomyRequestForm	&operator=(const RobotomyRequestForm &assign);

	virtual void	action() const;
};

std::ostream	&operator<<(std::ostream &os, RobotomyRequestForm &object);

#endif
