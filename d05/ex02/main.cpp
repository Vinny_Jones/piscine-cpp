/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 11:59:38 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:12:47 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

void	runTest();

int main () {
	runTest();
	return (0);
}

void 	runTest() {
	ShrubberyCreationForm	a("Shrubbery target");
	RobotomyRequestForm		b("Robotomy target");
	PresidentialPardonForm	c("PresidentialPardon target");
	Bureaucrat b1("Cool guy", 100);
	Bureaucrat b2("Intern (no one)", 140);
	Bureaucrat b3("Super bureaucrat", 40);
	Bureaucrat b4("God Of Bureaucrats", 1);

	std::cout << a << std::endl;

	a.beSigned(b1);
	/* Caght exception due too low range*/
	try {
		a.execute(b2);
	} catch (std::exception &ex) {
		std::cerr << "[0]Caught exception: " << ex.what() << std::endl;
	}
	/* Execute form by b1 is OK */
	try {
		a.execute(b1);
	} catch (std::exception &ex) {
		std::cerr << "[1]Caught exception: " << ex.what() << std::endl;
	}
	std::cout << a << std::endl;

	b.beSigned(b3);
	/* Execute form b by b1 is fail due to low grade */
	try {
		b.execute(b1);
	} catch (std::exception &ex) {
		std::cerr << "[2]Caught exception: " << ex.what() << std::endl;
	}
	/* b3 is OK */
	try {
		b.execute(b3);
	} catch (std::exception &ex) {
		std::cerr << "[3]Caught exception: " << ex.what() << std::endl;
	}
	std::cout << b << std::endl;

	c.beSigned(b4);
	try {
		c.execute(b3);
	} catch (std::exception &ex) {
		std::cerr << "[4]Caught exception: " << ex.what() << std::endl;
	}

	try {
		c.execute(b4);
	} catch (std::exception &ex) {
		std::cerr << "[5]Caught exception: " << ex.what() << std::endl;
	}
	std::cout << c << std::endl;
}
