/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:11:21 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:11:22 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"

const int ShrubberyCreationForm::requiredSignGrade = 145;
const int ShrubberyCreationForm::requiredExecGrade = 137;

ShrubberyCreationForm::ShrubberyCreationForm(const std::string &target)
	: Form(target, ShrubberyCreationForm::requiredSignGrade, ShrubberyCreationForm::requiredExecGrade)
{}


ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm &copy) : Form(copy) {}

ShrubberyCreationForm::~ShrubberyCreationForm() {}

ShrubberyCreationForm	&ShrubberyCreationForm::operator=(const ShrubberyCreationForm &assign) {
	Form::operator=(assign);
	return (*this);
}

void	ShrubberyCreationForm::action() const {
	std::string		filename = getName() + "_shrubbery";
	std::ofstream	file(filename.c_str(), std::ofstream::out | std::ofstream::trunc);

	if (!file.is_open()) {
		std::cerr << "Error while opening file" << std::endl;
		return ;
	}

	std::string		output;
	output.append("                     __            \n");
	output.append("                   _______         \n");
	output.append("                ((__     ___       \n");
	output.append("              (((          __      \n");
	output.append("            ((    o         ))     \n");
	output.append("            (     o    o     ))    \n");
	output.append("            (     o    o      )    \n");
	output.append("            (  o              )    \n");
	output.append("           ((  o     o   o    )    \n");
	output.append("          ((   o     o   o   )     \n");
	output.append("         ((          o       )     \n");
	output.append("        (                 o   )    \n");
	output.append("        (   o     *  *    o   )    \n");
	output.append("        ((  o  o  * **       ))    \n");
	output.append("         (     o ** *       ))     \n");
	output.append("          (      *  *  o   )       \n");
	output.append("          (     **  *  o  o)       \n");
	output.append("         ((     *   *     )        \n");
	output.append("          (    **   *    ))        \n");
	output.append("          ((   *    * ))))         \n");
	output.append("            ( **    *))            \n");
	output.append("             **     *              \n");
	output.append("            **      **             \n");
	output.append("           **        *             \n");
	output.append("#        **          **        ####\n");
	output.append("####   ***            *  ##########\n");
	output.append("###################################\n");

	file << output << output << output;
	file.close();
}

std::ostream	&operator<<(std::ostream &os, ShrubberyCreationForm &object) {
	Form	*base = dynamic_cast<Form *>(&object);
	os << "ShrubberyCreation " << *base;
	return (os);
}
