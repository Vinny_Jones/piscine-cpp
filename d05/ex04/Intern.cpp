/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:16:12 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:40:44 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"

const std::string			Intern::robotomy = "robotomy request";
const std::string			Intern::shrubbery = "shrubbery creation";
const std::string			Intern::presidential = "presidential pardon";
const Intern::UnknownForm	Intern::unknownFormException = Intern::UnknownForm();

const char*	Intern::UnknownForm::what() const throw() {
	return ("Unknown form.");
}

Intern::Intern() {}

Intern::Intern(const Intern &) {}

Intern::~Intern() {}

Intern	&Intern::operator=(const Intern &) {
	return (*this);
}

Form	*Intern::makeForm(const std::string &form, const std::string &target) {
	if (form == Intern::robotomy) {
		std::cout << "Intern creates RobotomyRequestForm." << std::endl;
		return (new RobotomyRequestForm(target));
	}
	if (form == Intern::shrubbery) {
		std::cout << "Intern creates ShrubberyCreationForm." << std::endl;
		return (new ShrubberyCreationForm(target));
	}
	if (form == Intern::presidential) {
		std::cout << "Intern creates PresidentialPardonForm." << std::endl;
		return (new PresidentialPardonForm(target));
	}
	std::cout << "Error! " << form << " form is not known." << std::endl;
	throw unknownFormException;
}
