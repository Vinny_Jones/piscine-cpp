/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:21:51 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:34:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OfficeBlock.hpp"

OfficeBlock::OfficeBlock() {
	m_intern = NULL;
	m_signer = NULL;
	m_executor = NULL;
	m_form = NULL;
}

OfficeBlock::OfficeBlock(const Intern &intern, const Bureaucrat &signer, const Bureaucrat &executor) :
 	m_intern(new Intern(intern)),
	m_signer(new Bureaucrat(signer)),
	m_executor(new Bureaucrat(executor)),
	m_form(NULL)
{}

/* private, do nothing */
OfficeBlock::OfficeBlock(const OfficeBlock &) {}

OfficeBlock::~OfficeBlock() {
	if (m_intern)
		delete m_intern;
	if (m_signer)
		delete m_signer;
	if (m_executor)
		delete m_executor;
	if (m_form)
		delete m_form;
}

/* private, do nothing */
OfficeBlock	&OfficeBlock::operator=(const OfficeBlock &) {
	return (*this);
}

void	OfficeBlock::doBureaucracy(const std::string &form, const std::string &target) {
	if (!m_intern || !m_signer || !m_executor)
		return;
	if (m_form)
		delete m_form;

	m_form = m_intern->makeForm(form, target);
	m_form->beSigned(*m_signer);
	m_form->execute(*m_executor);
}

void	OfficeBlock::setIntern(const Intern &intern) {
	if (m_intern)
		delete m_intern;
	m_intern = new Intern(intern);
}

void	OfficeBlock::setSigner(const Bureaucrat &signer) {
	if (m_signer)
		delete m_signer;
	m_signer = new Bureaucrat(signer);
}

void	OfficeBlock::setExecutor(const Bureaucrat &executor) {
	if (m_executor)
		delete m_executor;
	m_executor = new Bureaucrat(executor);
}
