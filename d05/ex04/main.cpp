/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:21:27 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:41:26 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OfficeBlock.hpp"

void	runTest();
void	doBureaucracyTest(OfficeBlock &ob, const std::string &form, const std::string &target);

int main () {
	runTest();
	return (0);
}

void 	runTest() {
	Intern		idiotOne;
	Bureaucrat	hermes = Bureaucrat("Hermes Conrad", 37);
	Bureaucrat	bob = Bureaucrat("Bobby Bobson", 123);
	Bureaucrat	rob = Bureaucrat("Robby Robson", 60);
	OfficeBlock	ob;

	ob.setIntern(idiotOne);
	ob.setSigner(bob);
	ob.setExecutor(hermes);

	doBureaucracyTest(ob, "wtf is going on here?", "Pigley");	/* test unknown form exception */
	doBureaucracyTest(ob, "presidential pardon", "Pigley");		/* test too low grade exception */
	doBureaucracyTest(ob, "shrubbery creation", "Pigley");		/* test all good */

	ob.setSigner(rob);
	ob.setExecutor(rob);

	doBureaucracyTest(ob, "robotomy request", "Pigley");		/* signed, but doesn't executed */
}

void	doBureaucracyTest(OfficeBlock &ob, const std::string &form, const std::string &target) {
	try
	{
		ob.doBureaucracy(form, target);
	} catch (Intern::UnknownForm &e) {
		std::cout << "[Exception occured] Intern: " << e.what() << std::endl;
	} catch (Bureaucrat::GradeTooHighException &e) {
		std::cout << "[Exception occured] Bureaucrat: " << e.what() << std::endl;
	} catch (Bureaucrat::GradeTooLowException &e) {
		std::cout << "[Exception occured] Bureaucrat: " << e.what() << std::endl;
	} catch (Form::GradeTooHighException &e) {
		std::cout << "[Exception occured] Form: " << e.what() << std::endl;
	} catch (Form::GradeTooLowException &e) {
		std::cout << "[Exception occured] Form: " << e.what() << std::endl;
	} catch (std::exception &e) {
		std::cout << "[Unknown exception occured]: " << e.what() << std::endl;
	}
}
