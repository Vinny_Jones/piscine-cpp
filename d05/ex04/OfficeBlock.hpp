/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:21:46 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:21:47 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OFFICEBLOCK_HPP
# define OFFICEBLOCK_HPP

# include "Intern.hpp"

class OfficeBlock {
public:
	OfficeBlock();
	OfficeBlock(const Intern &intern, const Bureaucrat &signBur, const Bureaucrat &execBur);
	virtual ~OfficeBlock();

	void	doBureaucracy(const std::string &form, const std::string &target);
	void	setIntern(const Intern &intern);
	void	setSigner(const Bureaucrat &signer);
	void	setExecutor(const Bureaucrat &executor);

private:
	OfficeBlock(const OfficeBlock &);
	OfficeBlock	&operator=(const OfficeBlock &);

	Intern		*m_intern;
	Bureaucrat	*m_signer;
	Bureaucrat	*m_executor;
	Form		*m_form;
};

#endif
