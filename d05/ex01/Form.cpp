/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 10:46:13 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 11:28:30 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.hpp"

const Form::GradeTooHighException	Form::tooHighException = Form::GradeTooHighException();
const Form::GradeTooLowException	Form::tooLowException = Form::GradeTooLowException();

const char*	Form::GradeTooHighException::what() const throw() {
	return ("Grade too high.");
}

const char*	Form::GradeTooLowException::what() const throw() {
	return ("Grade too low.");
}

Form::Form(const std::string &name,	int signGrade, int executeGrade)
	: m_name(name), m_signGrade(signGrade), m_executeGrade(executeGrade)
{
	if (signGrade < Bureaucrat::highestGrade || executeGrade < Bureaucrat::highestGrade)
		throw (tooHighException);
	else if (signGrade > Bureaucrat::lowestGrade || executeGrade > Bureaucrat::lowestGrade)
		throw (tooLowException);
	m_isSigned = false;
}

Form::Form(const Form &copy) : m_name(copy.m_name),
	m_signGrade(copy.m_signGrade), m_executeGrade(copy.m_executeGrade)
{
	*this = copy;
}

Form::~Form() {}

Form	&Form::operator=(const Form &assign) {
	m_isSigned = assign.m_isSigned;
	return (*this);
}

const std::string	&Form::getName() const {
	return (m_name);
}

int		Form::getSignGrade() const {
	return (m_signGrade);
}

int		Form::getExecuteGrade() const {
	return (m_executeGrade);
}

bool	Form::isSigned() const {
	return (m_isSigned);
}

void	Form::beSigned(const Bureaucrat &b) {
	if (m_isSigned)
		return ;
	if (!b.signForm(*this)) {
		throw (tooLowException);
	}
	m_isSigned = true;
}

std::ostream&	operator<<(std::ostream &os, Form &object) {
	os << "Form \"" << object.getName() << "\" is " << (object.isSigned() ? "signed" : "not signed")
	<< ". Sign grade - " << object.getSignGrade() << ". Execute grade - " << object.getExecuteGrade();
	return (os);
}
