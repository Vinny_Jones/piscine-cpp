/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 10:46:37 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 10:46:38 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.hpp"

void	runTest();

int main () {
	runTest();
	return (0);
}

void 	runTest() {
	Form *a;
	Bureaucrat b1("Cool guy", 30);
	Bureaucrat b2("Intern (no one)", 130);

	std::cout << "Trying to create new form" << std::endl;
	try {
		a = new Form("test form", 1, 155);
	} catch (std::exception &ex) {
		std::cerr << "Caught exception: " << ex.what() << std::endl;
		a = new Form(); /* in case of error - create default object */
	}

	std::cout << "Form a is: " << std::endl << *a << std::endl;
	/* try to reassign form to have normal values */
	try {
		delete a;
		a = new Form("test form", 50, 100);
	} catch (std::exception &ex) {
		std::cerr << "Caught exception: " << ex.what() << std::endl;
		a = NULL;
	}

	if (a)
		std::cout << "Form a is now: " << std::endl << *a << std::endl;
	else
		return ;

	try {
		a->beSigned(b2);
	} catch (std::exception &ex) {
		std::cerr << "Caught exception: " << ex.what() << std::endl;
	}

	try {
		a->beSigned(b1);
	} catch (std::exception &ex) {
		std::cerr << "Caught exception: " << ex.what() << std::endl;
	}
	std::cout << "Result: " << std::endl << *a << std::endl;
	delete a;
}
