/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 10:49:39 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 11:43:21 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.hpp"

const Bureaucrat::GradeTooHighException	Bureaucrat::tooHighException = Bureaucrat::GradeTooHighException();
const Bureaucrat::GradeTooLowException	Bureaucrat::tooLowException = Bureaucrat::GradeTooLowException();
const int		Bureaucrat::highestGrade = 1;
const int		Bureaucrat::lowestGrade = 150;

const char*	Bureaucrat::GradeTooHighException::what() const throw() {
	return ("Grade too high.");
}

const char*	Bureaucrat::GradeTooLowException::what() const throw() {
	return ("Grade too low.");
}

Bureaucrat::Bureaucrat(const std::string &name, int grade) : m_name(name) {
	setGrade(grade);
}

Bureaucrat::Bureaucrat(const Bureaucrat &copy) : m_name(copy.m_name) {
	*this = copy;
}

Bureaucrat::~Bureaucrat() {}

Bureaucrat&	Bureaucrat::operator=(const Bureaucrat &assign) {
	m_grade = assign.m_grade;
	return (*this);
}

int		Bureaucrat::getGrade() const {
	return (m_grade);
}

void Bureaucrat::setGrade(int value) {
	if (value < Bureaucrat::highestGrade)
		throw (tooHighException);
	else if (value > Bureaucrat::lowestGrade)
		throw (tooLowException);
	m_grade = value;
}

const std::string	&Bureaucrat::getName() const {
	return (m_name);
}

void	Bureaucrat::incrementGrade() {
	setGrade(m_grade - 1);
}

void	Bureaucrat::decrementGrade() {
	setGrade(m_grade + 1);
}

bool	Bureaucrat::signForm(Form &target) const {
	if (m_grade > target.getSignGrade()) {
		std::cout << m_name <<  " cannot sign \"" << target.getName();
		std::cout << "\" because bureaucrat grade too low." << std::endl;
		return (false);
	}
	std::cout << m_name <<  " signs " << target.getName() << std::endl;
	return (true);
}

std::ostream	&operator<<(std::ostream &os, Bureaucrat object) {
	os << object.getName() << ", bureaucrat grade " << object.getGrade();
	return (os);
}
