/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 10:44:01 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 11:46:18 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
# define FORM_HPP

# include "Bureaucrat.hpp"

class Form {
public:
	struct GradeTooHighException : public std::exception {
		virtual const char* what() const throw();
	};

	struct GradeTooLowException : public std::exception {
		virtual const char* what() const throw();
	};

	static const GradeTooHighException	tooHighException;
	static const GradeTooLowException	tooLowException;

	Form(const std::string &name = std::string("[no name]"),
		int signGrade = Bureaucrat::lowestGrade,
		int executeGrade = Bureaucrat::lowestGrade);
	Form(const Form &copy);
	virtual ~Form();
	Form	&operator=(const Form &assign);

	const std::string	&getName() const;
	int					getSignGrade() const;
	int					getExecuteGrade() const;
	bool				isSigned() const;
	void				beSigned(const Bureaucrat &b);

private:
	const std::string	m_name;
	const int			m_signGrade;
	const int			m_executeGrade;
	bool				m_isSigned;
};

std::ostream	&operator<<(std::ostream &os, Form &object);

#endif
