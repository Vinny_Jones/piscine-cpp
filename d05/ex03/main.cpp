/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:16:37 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:18:32 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"

void	runTest();

int main () {
	runTest();
	return (0);
}

void 	runTest() {
	Bureaucrat b1("Cool guy", 1);
	Form	*form;
	Intern	thatGuy;

	form = thatGuy.makeForm("robotomy request", "test robotomy");
	if (form) {
		std::cout << *form << std::endl;
		form->beSigned(b1);
		form->execute(b1);
		delete form;
	}
	form = thatGuy.makeForm("presidential pardon", "test pres");
	if (form) {
		std::cout << *form << std::endl;
		form->beSigned(b1);
		form->execute(b1);
		delete form;
	}
	form = thatGuy.makeForm("blabla", "blabla");
}
