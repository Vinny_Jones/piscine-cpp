/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:11:05 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:11:06 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.hpp"

const int RobotomyRequestForm::requiredSignGrade = 72;
const int RobotomyRequestForm::requiredExecGrade = 45;

RobotomyRequestForm::RobotomyRequestForm(const std::string &target)
	: Form(target, RobotomyRequestForm::requiredSignGrade, RobotomyRequestForm::requiredExecGrade)
{}


RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm &copy) : Form(copy) {}

RobotomyRequestForm::~RobotomyRequestForm() {}

RobotomyRequestForm	&RobotomyRequestForm::operator=(const RobotomyRequestForm &assign) {
	Form::operator=(assign);
	return (*this);
}

void	RobotomyRequestForm::action() const {
	std::cout << "*some drilling noises* ";
	std::cout << getName();
	std::cout << (clock() % 2 ? " has been robotomized successfully" : " fail to robotomize");
	std::cout << std::endl;
}

std::ostream	&operator<<(std::ostream &os, RobotomyRequestForm &object) {
	Form	*base = dynamic_cast<Form *>(&object);
	os << "RobotomyRequest " << *base;
	return (os);
}
