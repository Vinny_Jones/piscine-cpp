/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 12:16:17 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 12:16:17 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERN_HPP
# define INTERN_HPP

# include "ShrubberyCreationForm.hpp"
# include "RobotomyRequestForm.hpp"
# include "PresidentialPardonForm.hpp"

class Intern {
public:
	static const std::string	robotomy;
	static const std::string	shrubbery;
	static const std::string	presidential;

	Intern();
	Intern(const Intern &);
	virtual ~Intern();
	Intern	&operator=(const Intern &);

	Form	*makeForm(const std::string &form, const std::string &target);
};

#endif
