/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 10:26:44 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 10:30:45 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

const Bureaucrat::GradeTooHighException	Bureaucrat::tooHighException = Bureaucrat::GradeTooHighException();
const Bureaucrat::GradeTooLowException	Bureaucrat::tooLowException = Bureaucrat::GradeTooLowException();
const int		Bureaucrat::highestGrade = 1;
const int		Bureaucrat::lowestGrade = 150;

const char*	Bureaucrat::GradeTooHighException::what() const throw() {
	return ("Grade too high.");
}

const char*	Bureaucrat::GradeTooLowException::what() const throw() {
	return ("Grade too low.");
}

Bureaucrat::Bureaucrat(const std::string &name, int grade) : m_name(name) {
	setGrade(grade);
}

Bureaucrat::Bureaucrat(const Bureaucrat &copy) : m_name(copy.m_name) {
	*this = copy;
}

Bureaucrat::~Bureaucrat() {}

Bureaucrat&	Bureaucrat::operator=(const Bureaucrat &assign) {
	m_grade = assign.m_grade;
	return (*this);
}

int		Bureaucrat::getGrade() const {
	return (m_grade);
}

void Bureaucrat::setGrade(int value) {
	if (value < Bureaucrat::highestGrade)
		throw (tooHighException);
	else if (value > Bureaucrat::lowestGrade)
		throw (tooLowException);
	m_grade = value;
}

const std::string	&Bureaucrat::getName() const {
	return (m_name);
}

void	Bureaucrat::incrementGrade() {
	setGrade(m_grade - 1);
}

void Bureaucrat::decrementGrade() {
	setGrade(m_grade + 1);
}

std::ostream&	operator<<(std::ostream &os, Bureaucrat object) {
	os << object.getName() << ", bureaucrat grade " << object.getGrade();
	return (os);
}
