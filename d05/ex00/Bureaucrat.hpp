/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 10:27:05 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 10:41:34 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

# include <iostream>
# include <exception>

class Bureaucrat {
public:
	struct GradeTooHighException : public std::exception {
		virtual const char* what() const throw();
	};

	struct GradeTooLowException : public std::exception {
		virtual const char* what() const throw();
	};

	static const GradeTooHighException	tooHighException;
	static const GradeTooLowException	tooLowException;
	static const int 					highestGrade;
	static const int					lowestGrade;

	Bureaucrat(const std::string &name = std::string("[no name]"), int grade = Bureaucrat::lowestGrade);
	Bureaucrat(const Bureaucrat &copy);
	virtual ~Bureaucrat();
	Bureaucrat	&operator=(const Bureaucrat &assign);

	int					getGrade() const;
	void 				setGrade(int value);
	const std::string	&getName() const;
	void 				incrementGrade();
	void 				decrementGrade();

private:
	const std::string	m_name;
	int 				m_grade;
};

std::ostream&	operator<<(std::ostream &os, Bureaucrat object);

#endif
