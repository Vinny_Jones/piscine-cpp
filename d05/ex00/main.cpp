/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 10:38:47 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/23 10:41:16 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

int 	main() {
	Bureaucrat a("test guy");

	for (int i = 0; i <= 151; ++i) {
		try {
			a.setGrade(i);
		}
		catch (std::exception &ex) {
			std::cerr << "Caught exception: " << ex.what() << " Tried to set grade value " << i << std::endl;
		}
	}
	std::cerr << a << std::endl;
	try {
		a.decrementGrade();
	} catch (std::exception &ex) {
		std::cerr << "Caught exception: " << ex.what() << " Grade value " << a.getGrade() << std::endl;
	}
	std::cerr << a << std::endl;

	try {
		a.setGrade(a.highestGrade);
		a.incrementGrade();
	} catch (std::exception &ex) {
		std::cerr << "Caught exception: " << ex.what() << " Grade value " << a.getGrade() << std::endl;
	}
	std::cerr << a << std::endl;
	return (0);
}
