/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 20:50:14 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/24 20:50:17 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template <class T>
void 	iter(T *arr, int len, void (*func)(T)) {
	for (int i = 0; i < len; ++i) {
		func(arr[i]);
	}
}

/* test code */

template <class T>
void 	square(T a) {
	std::cout << a * a << std::endl;
}

int 	main() {

	int		intArray[] = {-3, 0, 1, 2, 5};
	iter(intArray, 5, square);
	std::cout << "===========" << std::endl;
	double	doubleArray[] = {-5.5, -3.4, 0.5, 0.25, 3.5};
	iter(doubleArray, 5, square);

	return (0);
}
