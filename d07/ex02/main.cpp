/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 20:58:47 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/24 20:58:51 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Array.hpp"
#include <zconf.h>

template <typename T>
struct test {
	static int	instance;

	test() : index(instance++) {}

	test(T val) : value(val), index(instance++) {
//		std::cout << index << " constructed" << std::endl;
	}

	test(const test &copy) : value(copy.value), index(instance++) {
//		std::cout << index << " copy constructed" << std::endl;
	}

	~test() {
//		std::cout << "instance " << index << " destructed" << std::endl;
	}

	test	&operator=(const test &copy) {
//		std::cout << "assignation operator called" << std::endl;
		value = copy.value;
		return (*this);
	}

	bool	operator<(const test &rhs) { return (value < rhs.value); }
	bool	operator==(const test &rhs) { return (value == rhs.value); }

	T			value;
	const int	index;
};

template <typename T>
int		test<T>::instance = 0;

template <typename T>
std::ostream	&operator<<(std::ostream &os, const test<T> &obj) {
	os << obj.value;
	return (os);
}

int main() {
	int 	*aaa = new int();
	std::cout << *aaa << std::endl;



	std::cout << "======== test case 1" << std::endl;

	Array<test<int> >	empty;
	std::cout << "size = " << empty.size() << std::endl;
	try {
		std::cout << empty[5] << std::endl;
	} catch (std::exception &ex) {
		std::cout << "Exception occured: "  << ex.what() << std::endl;
	}

	std::cout << "======== test case 2" << std::endl;

	Array<test<int> >	a(5);
	std::cout << "size = " << a.size() << std::endl;

	std::cout << "======== test case 3" << std::endl;

	Array<test<int> >	b(a);
	std::cout << "size a = " << a.size() << std::endl;
	std::cout << "size b = " << b.size() << std::endl;
	std::cout << "A:" << std::endl;
	for (int i = 0; i < a.size(); ++i) {
		std::cout << a[i] << " ";
	}
	std::cout << std::endl << "B: " << std::endl;
	for (int i = 0; i < b.size(); ++i) {
		std::cout << b[i] << " ";
	}

	std::cout << std::endl << "======== test case 4" << std::endl;

	for (int i = 0; i < b.size(); ++i) {
		b[i] = i + 1;
	}
	std::cout << "A: " << std::endl;
	for (int i = 0; i < a.size(); ++i) {
		std::cout << a[i] << " ";
	}
	std::cout << std::endl << "B: " << std::endl;
	for (int i = 0; i < b.size(); ++i) {
		std::cout << b[i] << " ";
	}

	std::cout << std::endl << "======== test case 5" << std::endl;

	Array<test<int> >	c;
	c = b;

	for (int i = 0; i < a.size(); ++i) {
		std::cout << a[i] << " ";
	}
	std::cout << std::endl << "B: " << std::endl;
	for (int i = 0; i < b.size(); ++i) {
		std::cout << b[i] << " ";
	}
	std::cout << std::endl << "C: " << std::endl;
	for (int i = 0; i < c.size(); ++i) {
		std::cout << c[i] << " ";
	}
	std::cout << std::endl;


	std::cout << "======== finish" << std::endl;

//	sleep(5000);
	return (0);
}
