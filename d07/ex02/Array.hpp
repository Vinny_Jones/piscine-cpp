/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 20:59:29 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/24 20:59:30 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_HPP
# define ARRAY_HPP

#include <iostream>

template <typename T>
class Array {
public:
	Array();
	Array(unsigned int n);
	Array(const Array &rhs);
	~Array();

	Array<T>	&operator=(const Array &rhs);
	T			&operator[](int index);

	int 	size() const;
	void 	clear();

private:
	int m_size;
	T	*m_array;
};

template <typename T>
Array<T>::Array() : m_size(0), m_array(NULL) {}

template <typename T>
Array<T>::Array(unsigned int n) : m_size(n) {
	m_array = new T[m_size];
}

template <typename T>
Array<T>::Array(const Array &rhs) {
	*this = rhs;
}

template <typename T>
Array<T>::~Array() {
	clear();
}

template <typename T>
Array<T>	&Array<T>::operator=(const Array &rhs) {
	clear();
	m_size = rhs.m_size;
	m_array = new T[m_size];
	for (int i = 0; i < m_size; ++i) {
		m_array[i] = rhs.m_array[i];
	}
	return (*this);
}

template <typename T>
T		&Array<T>::operator[](int index) {
	if (index < 0 || index >= m_size)
		throw std::logic_error("Array index out of bounds");
	return (m_array[index]);
}

template <typename T>
int		Array<T>::size() const {
	return (m_size);
}

template <typename T>
void	Array<T>::clear() {
	delete [] m_array;
	m_array = NULL;
	m_size = 0;
}

#endif
