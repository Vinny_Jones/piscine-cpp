/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whatever.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 20:47:43 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/24 20:48:37 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template <class T>
void	swap(T &a, T &b) {
	T	temp = a;

	a = b;
	b = temp;
}

template <class T>
T	&min(T &a, T &b) {
	return (a < b ? a : b);
}

template <class T>
T	&max(T &a, T &b) {
	return (min(b, a) == a ? b : a);
}


/* test code below */

template <typename T>
struct test {
	static int	instance;

	test() : index(instance++) {}

	test(T val) : value(val), index(instance++) {
//		std::cout << index << " constructed" << std::endl;
	}

	test(const test<T> &copy) : value(copy.value), index(instance++) {
//		std::cout << index << " copy constructed" << std::endl;
	}

	~test() {
//		std::cout << "instance " << index << " destructed" << std::endl;
	}

	test	&operator=(const test<T> &copy) {
//		std::cout << "assignation operator called" << std::endl;
		value = copy.value;
		return (*this);
	}

	bool	operator<(const test<T> &rhs) {
		return (value < rhs.value);
	}
	bool	operator==(const test<T> &rhs) {
		return (value == rhs.value);
	}

	T			value;
	const int	index;
};

template <typename T>
std::ostream	&operator<<(std::ostream &os, const test<T> &obj) {
	os << obj.value;
	return (os);
}

template <typename T>
int test<T>::instance = 0;

int		main() {
	test<int> a = 33;
	test<int> b = 33;
	test<int> c = 44;

	std::cout << "a = " << a << " instance = " << a.index << std::endl;
	std::cout << "b = " << b << " instance = " << b.index <<std::endl;
	std::cout << "c = " << c << " instance = " << c.index <<std::endl;
	std::cout << "min(a, b) instance = " << min(a, b).index << std::endl;
	std::cout << "max(a, b) instance = " << max(a, b).index << std::endl;
	std::cout << "min(b, a) instance = " << min(b, a).index << std::endl;
	std::cout << "max(b, a) instance = " << max(b, a).index << std::endl;
	std::cout << "min(a, c) instance = " << min(a, c).index << std::endl;
	std::cout << "max(a, c) instance = " << max(a, c).index << std::endl;

	::swap(a, c);

	std::cout << "min(a, c) instance = " << min(a, c).index << std::endl;
	std::cout << "max(a, c) instance = " << max(a, c).index << std::endl;


	std::string d = "chaine1";
	std::string e = "chaine2";

	::swap(d, e);

	std::cout << "d = " << d << ", e = " << e << std::endl;
	std::cout << "min(d, e) = " << ::min(d, e) << std::endl;
	std::cout << "max(d, e) = " << ::max(d, e) << std::endl;


	return (0);
}
