/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneEntry.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 20:17:13 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/15 20:17:17 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEENTRY_HPP
# define PHONEENTRY_HPP

#include <limits>
#include <string>
#include <iomanip>
#include <iostream>

class PhoneEntry {
public:
	PhoneEntry();
	PhoneEntry(const PhoneEntry &copy);
	virtual ~PhoneEntry();
	PhoneEntry	&operator=(const PhoneEntry &assign);

	bool				isEmpty() const;
	void 				fillData();
	void 				clear();
	void 				print() const;
	const std::string	getFirstName() const;
	const std::string	getLastName() const;
	const std::string	getNickName() const;

private:
	bool			getInputData(const std::string &name, std::string &field);
	bool			m_empty;
	std::string		firstName;
	std::string		lastName;
	std::string		nickname;
	std::string		login;
	std::string		postAddress;
	std::string		emailAddress;
	unsigned long	phoneNumber;
	std::string		birthDay;
	std::string		meal;
	std::string		underwearColor;
	std::string		secret;
};

#endif
