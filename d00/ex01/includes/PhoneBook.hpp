/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 20:16:57 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/15 20:17:07 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOK_HPP
# define PHONEBOOK_HPP

# include "PhoneEntry.hpp"
# define ARRAY_SIZE 8

class PhoneBook {
public:
	PhoneBook();
	PhoneBook(const PhoneBook &copy);
	virtual ~PhoneBook();
	PhoneBook	&operator=(const PhoneBook &assign);

	bool		resolveCommand(std::string&);
	void		displayPreview(void) const;

private:
	void		addEntry(void);
	void		searchEntry(void) const;
	void		strToUpper(std::string &input);
	void		printPreviewColumn(const std::string &input) const;
	int			getEntryNumber(const std::string &name) const;

	PhoneEntry	data[ARRAY_SIZE];
};

#endif
