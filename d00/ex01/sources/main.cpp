/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 20:16:38 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/15 20:16:42 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PhoneBook.hpp"

int		main()
{
	PhoneBook	phoneBook;
	std::string	input;

	do {
		std::cout << "Enter your command: ";
		std::cin.clear();
		std::getline(std::cin, input);
		if (!std::cin.good()) {
			std::cout << std::endl << "Input error. Exiting..." << std::endl;
			return (0);
		}
	} while (phoneBook.resolveCommand(input) && std::cin.good());
	return (0);
}
