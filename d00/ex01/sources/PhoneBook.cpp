/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 20:11:52 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/15 20:12:04 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PhoneBook.hpp"

PhoneBook::PhoneBook() {}

PhoneBook::PhoneBook(const PhoneBook &copy) {
	*this = copy;
}

PhoneBook::~PhoneBook() {}

PhoneBook& PhoneBook::operator=(const PhoneBook &assign) {
	for (int i = 0; i < ARRAY_SIZE; ++i) {
		data[i] = assign.data[i];
	}
	return (*this);
}

void	PhoneBook::strToUpper(std::string &input) {
	for (unsigned long i = 0; i < input.length(); ++i) {
		input[i] = (char)std::toupper(input[i]);
	}
}

bool	PhoneBook::resolveCommand(std::string &command) {
	strToUpper(command);
	if (command == "ADD")
		addEntry();
	else if (command == "SEARCH")
		searchEntry();
	else if (command == "EXIT")
		return (false);
	else if (!command.empty())
		std::cout << "UNKNOWN command: \"" << command << "\"" << std::endl;
	return (true);
}

void	PhoneBook::addEntry() {
	int 	n;

	for (n = 0; n < ARRAY_SIZE && !data[n].isEmpty(); ++n) {
	}
	if (n == ARRAY_SIZE) {
		std::cout << "Phonebook is full. ";
		if((n = getEntryNumber("replace")) == -1)
			return ;
	}
	data[n].fillData();
}

void	PhoneBook::searchEntry() const {
	int		n;

	displayPreview();
	if((n = getEntryNumber("search")) == -1)
		return ;
	if (!data[n].isEmpty()) {
		std::cout << "===============Printing entry " << n << std::endl;
		data[n].print();
		std::cout << "==============================" << std::endl;
	}
	else {
		std::cout << "Entry index " << n << " is empty. Try again" << std::endl;
		searchEntry();
	}
}

int		PhoneBook::getEntryNumber(const std::string &name) const {
	int		n = -1;

	while (n < 0 || n > ARRAY_SIZE) {
		std::cout << "Enter entry index to " << name << " (0 to " << ARRAY_SIZE - 1 << ")";
		std::cout << " or " << ARRAY_SIZE << " to exit: ";
		std::cin >> n;
		if (std::cin.eof() || std::cin.bad()) {
			std::cout << std::endl << "Input error, " << name << " NOT performed!" << std::endl;
			return (-1);
		}
		if (std::cin.fail())
			n = -1;
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		if (n >= 0 && n < ARRAY_SIZE)
			break ;
		else if (n == ARRAY_SIZE)
			return (-1);
		else
			std::cout << "[Error]Wrong input!!! ";
	}
	return (n);
}

void	PhoneBook::displayPreview() const {
	for (int i = 0; i < ARRAY_SIZE; i++) {
		if (!data[i].isEmpty()) {
			std::cout << "|" << std::setw(10) << i << "|";
			printPreviewColumn(data[i].getFirstName());
			printPreviewColumn(data[i].getLastName());
			printPreviewColumn(data[i].getNickName());
			std::cout << std::endl;
		}
	}
}

void	PhoneBook::printPreviewColumn(const std::string &input) const {
	if (input.length() > 10)
		std::cout << input.substr(0, 9) << ".";
	else
		std::cout << std::setw(10) << input;
	std::cout << "|";
}
