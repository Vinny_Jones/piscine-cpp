/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneEntry.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 20:16:04 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/15 20:16:32 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PhoneEntry.hpp"

PhoneEntry::PhoneEntry() {
	clear();
	m_empty = true;
}

PhoneEntry::PhoneEntry(const PhoneEntry &copy) {
	*this = copy;
}

PhoneEntry::~PhoneEntry() {}

PhoneEntry	&PhoneEntry::operator=(const PhoneEntry &assign) {
	firstName = assign.firstName;
	lastName = assign.lastName;
	nickname = assign.nickname;
	login = assign.login;
	postAddress = assign.postAddress;
	emailAddress = assign.emailAddress;
	phoneNumber = assign.phoneNumber;
	birthDay = assign.birthDay;
	meal = assign.meal;
	underwearColor = assign.underwearColor;
	secret = assign.secret;
	m_empty = assign.m_empty;
	return (*this);
}

bool	PhoneEntry::isEmpty() const {
	return (m_empty);
}

void	PhoneEntry::clear() {
	firstName.clear();
	lastName.clear();
	nickname.clear();
	login.clear();
	postAddress.clear();
	emailAddress.clear();
	phoneNumber = 0;
	birthDay.clear();
	meal.clear();
	underwearColor.clear();
	secret.clear();
	m_empty = true;
}

void	PhoneEntry::print() const {
	std::cout << std::left << std::setw(17) << "First name: " << firstName << std::endl;
	std::cout << std::left << std::setw(17) << "Last name: " << lastName << std::endl;
	std::cout << std::left << std::setw(17) << "Nickname: " << nickname << std::endl;
	std::cout << std::left << std::setw(17) << "Login: " << login << std::endl;
	std::cout << std::left << std::setw(17) << "Post address: " << postAddress << std::endl;
	std::cout << std::left << std::setw(17) << "Email address: " << emailAddress << std::endl;
	std::cout << std::left << std::setw(17) << "Phone number: " << phoneNumber << std::endl;
	std::cout << std::left << std::setw(17) << "Birthday: " << birthDay << std::endl;
	std::cout << std::left << std::setw(17) << "Favourite meal: " << meal << std::endl;
	std::cout << std::left << std::setw(17) << "Underwear color: " << underwearColor << std::endl;
	std::cout << std::left << std::setw(17) << "Darkest secret: " << secret << std::endl;
}

void	PhoneEntry::fillData() {
	clear();
	std::string		temp;
	std::cout << ">>>>>> Provide data for phone entry" << std::endl;
	if (!getInputData("First name: ", firstName))
		return ;
	if (!getInputData("Last name: ", lastName))
		return ;
	if (!getInputData("Nickname: ", nickname))
		return ;
	if (!getInputData("Login: ", login))
		return ;
	if (!getInputData("Post address: ", postAddress))
		return ;
	if (!getInputData("Email address: ", emailAddress))
		return ;
	while (phoneNumber == 0) {
		std::cout << "Phone number: ";
		std::cin >> phoneNumber;
		if (std::cin.eof() || std::cin.bad()) {
			std::cout << "Input error. Phone entry NOT saved!" << std::endl;
			return ;
		}
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		if (phoneNumber == 0)
			std::cout << "Invalid phone number. Try again. ";
	}
	if (!getInputData("Birthday: ", birthDay))
		return ;
	if (!getInputData("Favourite meal: ", meal))
		return ;
	if (!getInputData("Underwear color: ", underwearColor))
		return ;
	if (!getInputData("Darkest secret: ", secret))
		return ;
	m_empty = false;
	std::cout << ">>>>>> Phone entry successfully saved" << std::endl;
}

bool	PhoneEntry::getInputData(const std::string &name, std::string &field) {
	while (field.empty()) {
		std::cout << name;
		std::getline(std::cin, field);
		if (!std::cin.good()) {
			std::cout << std::endl << "Input error. Phone entry NOT saved!" << std::endl;
			return (false);
		}
		if (field.empty())
			std::cout << "Can't be empty. Enter valid data. ";
	}
	return (true);
}

const std::string	PhoneEntry::getFirstName() const {
	return (firstName);
}

const std::string	PhoneEntry::getLastName() const {
	return (lastName);
}

const std::string	PhoneEntry::getNickName() const {
	return (nickname);
}