/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 19:55:01 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/15 19:56:09 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#define DEFAULT "* LOUD AND UNBEARABLE FEEDBACK NOISE *"

int		main(int argc, const char *argv[])
{
	if (argc == 1)
		std::cout << DEFAULT;
	else
		for (int i = 1; i < argc; ++i) {
			for (unsigned long j = 0; argv[i][j]; ++j) {
				std::putchar(std::toupper(argv[i][j]));
			}
		}
	std::cout << std::endl;
	return (0);
}
