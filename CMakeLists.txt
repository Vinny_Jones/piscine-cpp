cmake_minimum_required(VERSION 3.8)
project(piscineCPP)
set(CMAKE_CXX_COMPILER "/usr/bin/g++")
set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror -std=c++98 -D_XOPEN_SOURCE_EXTENDED" )
#separate directive for includes (if needed)
include_directories(d00/ex01/includes rush00/inc)

set(MEGAPHONE_SRC d00/ex00/megaphone.cpp)
add_executable(megaphone ${MEGAPHONE_SRC})

set(D00EX01SRC d00/ex01/sources/main.cpp d00/ex01/sources/PhoneEntry.cpp d00/ex01/sources/PhoneBook.cpp)
add_executable(phonebook ${D00EX01SRC})

set(D01EX00SRC d01/ex00/main.cpp d01/ex00/Pony.cpp d01/ex00/Pony.hpp)
add_executable(pony ${D01EX00SRC})

set(D01EX01SRC d01/ex01/ex01.cpp)
add_executable(d01ex01 ${D01EX01SRC})

set(D01EX02SRC d01/ex02/main.cpp d01/ex02/Zombie.cpp d01/ex02/ZombieEvent.cpp)
add_executable(zombie ${D01EX02SRC})

set(D01EX03SRC d01/ex03/main.cpp d01/ex03/ZombieHorde.cpp d01/ex03/Zombie.cpp)
add_executable(zombieHorde ${D01EX03SRC})

set(D01EX04SRC d01/ex04/ex04.cpp)
add_executable(brain ${D01EX04SRC})

set(D01EX05SRC d01/ex05/main.cpp d01/ex05/Brain.cpp d01/ex05/Human.cpp)
add_executable(humanBrain ${D01EX05SRC})

set(D01EX06SRC d01/ex06/main.cpp d01/ex06/Weapon.cpp d01/ex06/HumanA.cpp d01/ex06/HumanB.cpp)
add_executable(violence ${D01EX06SRC})

set(D02EX00SRC d02/ex00/TEMP_main.cpp d02/ex00/Fixed.cpp)
add_executable(fixed00 ${D02EX00SRC})

set(D02EX01SRC d02/ex01/TEMP_main.cpp d02/ex01/Fixed.cpp)
add_executable(fixed01 ${D02EX01SRC})

set(D02EX02SRC d02/ex02/TEMP_main.cpp d02/ex02/Fixed.cpp)
add_executable(fixed02 ${D02EX02SRC})

set(D02EX04SRC d02/ex04/main.cpp d02/ex04/Fixed.cpp d02/ex04/Eval_expr.cpp)
add_executable(eval_expr ${D02EX04SRC})

set(D03EX00SRC d03/ex00/main.cpp d03/ex00/FragTrap.cpp)
add_executable(d03ex00 ${D03EX00SRC})

set(D03EX01SRC d03/ex01/main.cpp d03/ex01/FragTrap.cpp d03/ex01/ScavTrap.cpp)
add_executable(d03ex01 ${D03EX01SRC})

set(D03EX02SRC d03/ex02/ClapTrap.cpp d03/ex02/FragTrap.cpp d03/ex02/ScavTrap.cpp d03/ex02/main.cpp)
add_executable(d03ex02 ${D03EX02SRC})

set(D03EX03SRC d03/ex03/main.cpp d03/ex03/ClapTrap.cpp d03/ex03/FragTrap.cpp d03/ex03/NinjaTrap.cpp d03/ex03/ScavTrap.cpp)
add_executable(d03ex03 ${D03EX03SRC})

set(D03EX04SRC d03/ex04/main.cpp d03/ex04/ClapTrap.cpp d03/ex04/FragTrap.cpp d03/ex04/NinjaTrap.cpp
        d03/ex04/ScavTrap.cpp d03/ex04/SuperTrap.cpp)
add_executable(d03ex04 ${D03EX04SRC})

set(D04EX00SRC d04/ex00/main.cpp d04/ex00/Sorcerer.cpp d04/ex00/Victim.cpp d04/ex00/Peon.cpp)
add_executable(d04ex00 ${D04EX00SRC})

set(D04EX01SRC d04/ex01/main.cpp d04/ex01/AWeapon.cpp d04/ex01/PlasmaRifle.cpp d04/ex01/PowerFist.cpp
        d04/ex01/Enemy.cpp d04/ex01/SuperMutant.cpp d04/ex01/RadScorpion.cpp d04/ex01/Character.cpp)
add_executable(d04ex01 ${D04EX01SRC})

#set(D04EX02SRC d04/ex02/ISquad.hpp  d04/ex02/ISpaceMarine.hpp d04/ex02/main.cpp d04/ex02/S)
AUX_SOURCE_DIRECTORY(d04/ex02 D04EX02SRC)
add_executable(d04ex02 ${D04EX02SRC})

set(RUSH00SRC rush00/src/main.cpp rush00/src/globalInfo.cpp rush00/inc/globalInfo.hpp rush00/inc/vObject.hpp
        rush00/src/vObject.cpp rush00/inc/vSpaceship.hpp rush00/src/vSpaceship.cpp rush00/inc/vUser.hpp
        rush00/src/vUser.cpp rush00/inc/objectManager.hpp rush00/src/objectManager.cpp rush00/inc/cList.hpp
        rush00/inc/cIntPair.hpp rush00/inc/vBullet.hpp rush00/src/vBullet.cpp rush00/inc/gameEngine.hpp
        rush00/src/gameEngine.cpp rush00/inc/vEnemy.hpp rush00/src/vEnemy.cpp rush00/inc/timeManager.hpp
        rush00/src/timeManager.cpp)
add_executable(rush00 ${RUSH00SRC})
target_link_libraries(rush00 libncurses.a)

AUX_SOURCE_DIRECTORY(d05/ex00 D05EX00SRC)
add_executable(d05ex00 ${D05EX00SRC})

AUX_SOURCE_DIRECTORY(d07/ex00 D07EX00SRC)
add_executable(d07ex00 ${D07EX00SRC})

AUX_SOURCE_DIRECTORY(d07/ex01 D07EX01SRC)
add_executable(d07ex01 ${D07EX01SRC})

set(D07EX02SRC d07/ex02/main.cpp d07/ex02/Array.hpp)
add_executable(d07ex02 ${D07EX02SRC})

set(D08EX00SRC d08/ex00/main.cpp d08/ex00/easyfind.hpp)
add_executable(d08ex00 ${D08EX00SRC})

set(D08EX01SRC d08/ex01/main.cpp d08/ex01/span.cpp d08/ex01/span.hpp)
add_executable(d08ex01 ${D08EX01SRC})