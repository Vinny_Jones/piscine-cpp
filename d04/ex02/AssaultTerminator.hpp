/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AssaultTerminator.hpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 20:42:13 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/19 20:42:21 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASSAULTTERMINATOR_HPP
# define ASSAULTTERMINATOR_HPP

# include "ISpaceMarine.hpp"

class AssaultTerminator : public ISpaceMarine {
public:
	AssaultTerminator();
	AssaultTerminator(const AssaultTerminator &rhs);
	virtual ~AssaultTerminator();
	AssaultTerminator	&operator=(const AssaultTerminator &rhs);

	virtual AssaultTerminator*	clone() const;
	virtual void				battleCry() const;
	virtual void				rangedAttack() const;
	virtual void				meleeAttack() const;
};

#endif
