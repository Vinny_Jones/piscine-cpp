#include "TacticalMarine.hpp"
#include "Squad.hpp"
#include "AssaultTerminator.hpp"

#include <zconf.h>

int 	run(void) {
	ISpaceMarine* bob = new TacticalMarine;
	ISpaceMarine* jim = new AssaultTerminator;
	ISpaceMarine* jim1 = new TacticalMarine;
	ISpaceMarine* jim2 = new AssaultTerminator;
	ISpaceMarine* jim3 = new AssaultTerminator;
	ISpaceMarine* jim4 = new TacticalMarine;

	ISquad* vlc = new Squad;
	vlc->push(bob);
	vlc->push(jim);
	vlc->push(jim1);
	vlc->push(jim2);
	vlc->push(jim3);
	vlc->push(jim4);
	for (int i = 0; i < vlc->getCount(); ++i)
	{
		ISpaceMarine* cur = vlc->getUnit(i);
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}

	delete vlc;
	return 0;
}

int	main() {
	run();
//	sleep(500);
	return (0);
}