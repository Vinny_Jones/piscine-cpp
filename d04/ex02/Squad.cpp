/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 18:47:42 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/19 18:47:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.hpp"

const int 	Squad::offset = 10;

Squad::Squad() {
	m_count = 0;
	m_maxElements = Squad::offset;
	m_container = new ISpaceMarine*[m_maxElements];
	for (int i = 0; i < m_maxElements; ++i) {
		m_container[i] = NULL;
	}
}

Squad::Squad(const Squad &rhs) {
	*this = rhs;
}

Squad::~Squad() {
	clear();
}

Squad	&Squad::operator=(const Squad &rhs) {
	clear();
	m_count = rhs.m_count;
	m_maxElements = rhs.m_maxElements;
	m_container = new ISpaceMarine*[m_maxElements];
	int		i;
	for (i = 0; i < m_count; ++i) {
		m_container[i] = rhs.m_container[i]->clone();
	}
	while (i < m_maxElements) {
		m_container[i++] = NULL;
	}
	return (*this);
}

int		Squad::getCount() const {
	return (m_count);
}

ISpaceMarine* Squad::getUnit(int index) const {
	if (!m_container || index < 0 || index >= m_count)
		return (NULL);
	return (m_container[index]);
}

int		Squad::push(ISpaceMarine *elem) {
	if (!elem)
		return (m_count);
	int		i;
	for (i = 0; i < m_count; ++i) {
		if (elem == m_container[i])
			return (m_count);
	}
	if (m_count == m_maxElements) {
		ISpaceMarine	**oldContainer = m_container;

		m_maxElements += Squad::offset;
		m_container = new ISpaceMarine*[m_maxElements];
		int		j;
		for (j = 0; j < m_count; ++j) {
			m_container[j] = oldContainer[j];
		}
		while (j < m_maxElements) {
			m_container[j++] = NULL;
		}
		delete [] oldContainer;
	}
	m_container[m_count++] = elem;
	return (m_count);
}

void	Squad::clear() {
	for (int i = 0; i < m_count; ++i) {
		delete m_container[i];
	}
	delete [] m_container;
	m_container = NULL;
	m_count = 0;
}