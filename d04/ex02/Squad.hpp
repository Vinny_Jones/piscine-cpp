/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 18:47:32 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/19 18:47:35 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUAD_HPP
# define SQUAD_HPP

# include "ISquad.hpp"

class Squad : public ISquad {
public:
	static const int	offset;

	Squad();
	Squad(const Squad &rhs);
	virtual ~Squad();
	Squad	&operator=(const Squad &rhs);

	virtual int getCount() const;
	virtual ISpaceMarine* getUnit(int index) const;
	virtual int push(ISpaceMarine*);

private:
	void			clear();
	int				m_count;
	int 			m_maxElements;
	ISpaceMarine	**m_container;
};

#endif
