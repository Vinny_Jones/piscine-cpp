//
// 42 header
//

#ifndef VICTIM_HPP
# define VICTIM_HPP

# include <iostream>

class Victim {
public:
	Victim(const std::string &name);
	Victim(const Victim &copy);
	virtual ~Victim();
	Victim				&operator=(const Victim &assign);
	const std::string	name() const;
	virtual void		getPolymorphed() const;
protected:
	Victim();
	std::string		m_name;
};

std::ostream	&operator<<(std::ostream &os, const Victim &obj);

#endif
