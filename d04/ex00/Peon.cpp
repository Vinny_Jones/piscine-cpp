//
// 42 header
//

#include "Peon.hpp"

Peon::Peon() {

}

Peon::Peon(const std::string &name) : Victim(name) {
	std::cout << "Zog zog." << std::endl;
}

Peon::Peon(const Peon &copy) {
	*this = copy;
}

Peon::~Peon() {
	std::cout << "Bleuark..." << std::endl;
}

Peon	&Peon::operator=(const Peon &assign) {
	m_name = assign.name();
	return (*this);
}

void	Peon::getPolymorphed() const {
	std::cout << m_name << " has been turned into a pink pony !" << std::endl;
}