//
// 42 header
//

#ifndef SORCERER_HPP
# define SORCERER_HPP

# include <iostream>
# include "Victim.hpp"

class Sorcerer {
public:
	Sorcerer(const std::string &name, const std::string &title);
	Sorcerer(const Sorcerer &copy);
	virtual ~Sorcerer();
	Sorcerer			&operator=(const Sorcerer &assign);
	const std::string	name() const;
	const std::string	&title() const;
	void				polymorph(Victim const &victim) const;
private:
	Sorcerer();
	std::string	m_name;
	std::string	m_title;
};

std::ostream	&operator<<(std::ostream &os, const Sorcerer &obj);

#endif