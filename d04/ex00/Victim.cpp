//
// 42 header
//

#include "Victim.hpp"

Victim::Victim() {
}

Victim::Victim(const std::string &name) : m_name(name) {
	std::cout << "Some random victim called " << m_name << " just popped !" << std::endl;
}

Victim::Victim(const Victim &copy) {
	*this = copy;
}

Victim::~Victim() {
	std::cout << "Victim " << m_name << " just died for no apparent reason !" << std::endl;
}

Victim	&Victim::operator=(const Victim &assign) {
	m_name = assign.m_name;
	std::cout << "[temp]copied" << std::endl;
	return (*this);
}

const std::string	Victim::name() const {
	return (m_name);
}

void	Victim::getPolymorphed() const {
	std::cout << m_name << " has been turned into a cute little sheep !" << std::endl;
}

std::ostream	&operator<<(std::ostream &os, const Victim &obj) {
	os << "I'm " << obj.name() << " and I like otters !" << std::endl;
	return (os);
}