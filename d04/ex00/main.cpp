//
// 42 header
//

#include "Sorcerer.hpp"
#include "Victim.hpp"
#include "Peon.hpp"

void	customTest(void);

int 	main() {
	Sorcerer	robert("Robert", "the Magnificent");
	Victim jim("Jimmy");
	Peon joe("Joe");
	std::cout << robert << jim << joe;
	robert.polymorph(jim);
	robert.polymorph(joe);

	std::cout << "========== custom test begin =======" << std::endl;
	customTest();
	std::cout << "========== custom test finish ======" << std::endl;
	return (0);
}

void	customTest(void) {
	Victim	*vicArr[2];
	vicArr[0] = new Victim("vic1");
	vicArr[1] = new Peon("vic2-peon");

	for (int i = 0; i < 2; ++i) {
		vicArr[i]->getPolymorphed();
	}

	delete vicArr[0];
	delete vicArr[1];
}