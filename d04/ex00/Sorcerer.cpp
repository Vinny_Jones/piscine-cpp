//
// 42 header
//

#include "Sorcerer.hpp"

Sorcerer::Sorcerer() {
}

Sorcerer::Sorcerer(const std::string &name, const std::string &title) : m_name(name), m_title(title) {
	std::cout << m_name << ", " << m_title << ", is born!" << std::endl;
}

Sorcerer::Sorcerer(const Sorcerer &copy) {
	*this = copy;
}

Sorcerer::~Sorcerer() {
	std::cout << m_name << ", " << m_title << ", is dead. Consequences will never be the same !" << std::endl;
}

Sorcerer& Sorcerer::operator=(const Sorcerer &assign) {
	m_name = assign.m_name;
	m_title = assign.m_title;
	return (*this);
}

const std::string	Sorcerer::name() const {
	return (m_name);
}

const std::string	&Sorcerer::title() const {
	return (m_title);
}

std::ostream	&operator<<(std::ostream &os, const Sorcerer &obj) {
	os << "I am " << obj.name() << ", " << obj.title() << ", and I like ponies !" << std::endl;
	return (os);
}

void	Sorcerer::polymorph(const Victim &victim) const {
	victim.getPolymorphed();
}