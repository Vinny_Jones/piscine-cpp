//
// 42 header
//

#ifndef AWEAPON_HPP
# define AWEAPON_HPP

#include <iostream>

class AWeapon {
public:
	AWeapon(const AWeapon &copy);
	AWeapon(std::string const &name, int apcost, int damage);
	virtual ~AWeapon();
	AWeapon				&operator=(const AWeapon &assign);
	const std::string	getName() const;
	int 				getAPCost() const;
	int 				getDamage() const;
	virtual void 		attack() const = 0;
protected:
	std::string	m_name;
	int 		m_apCost;
	int 		m_damage;
private:
	AWeapon();
};

#endif