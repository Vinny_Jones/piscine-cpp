//
// 42 header
//

#ifndef RADSCORPION_HPP
# define RADSCORPION_HPP

# include "Enemy.hpp"

class RadScorpion : public Enemy {
public:
	RadScorpion();
	virtual ~RadScorpion();
};

#endif