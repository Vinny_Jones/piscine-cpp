//
// 42 header
//

#include "Character.hpp"

Character::Character() {
	/* pirvate constructor */
}

Character::Character(const std::string &name) : m_name(name), m_ap(40) {
	m_weapon = nullptr;
}

Character::Character(const Character &copy) {
	*this = copy;
}

Character::~Character() {

}

Character	&Character::operator=(const Character &assign) {
	m_name = assign.m_name;
	m_ap = assign.m_ap;
	m_weapon = assign.m_weapon;
	return (*this);
}

void	Character::recoverAP() {
	m_ap = (m_ap < 30) ? m_ap + 10 : 40;
}

void	Character::equip(AWeapon *newWeapon) {
	m_weapon = newWeapon;
}

void	Character::attack(Enemy *target) {
	if (!m_weapon || m_ap < m_weapon->getAPCost())
		return;
	std::cout << m_name << " attacks " << target->getType() << " with a " << m_weapon->getName() << std::endl;
	m_weapon->attack();
	m_ap -= m_weapon->getAPCost();
	target->takeDamage(m_weapon->getDamage());
	if (target->getHP() <= 0)
		delete target;
}

int		Character::getAP() const {
	return (m_ap);
}

const std::string	Character::getName() const {
	return (m_name);
}

AWeapon *Character::getWeapon() const {
	return (m_weapon);
}

std::ostream		&operator<<(std::ostream &os, const Character &obj) {
	os << obj.getName() << " has " << obj.getAP() << " AP and ";
	if (obj.getWeapon())
		os << "wields a " << obj.getWeapon()->getName() << std::endl;
	else
		os << "is unarmed" << std::endl;
	return (os);
}