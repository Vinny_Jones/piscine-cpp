//
// 42 header
//

#ifndef CHARACTER_HPP
# define CHARACTER_HPP

# include <iostream>
# include "AWeapon.hpp"
# include "Enemy.hpp"

class Character {
public:
	Character(const std::string &name);
	Character(const Character &copy);
	virtual ~Character();
	Character			&operator=(const Character &assign);
	void				recoverAP();
	void				equip(AWeapon *newWeapon);
	void				attack(Enemy *target);
	const std::string	getName() const;
	int					getAP() const;
	AWeapon				*getWeapon() const;
private:
	Character();
	std::string	m_name;
	int 		m_ap;
private:
	AWeapon		*m_weapon;
};

std::ostream		&operator<<(std::ostream &os, const Character &obj);

#endif