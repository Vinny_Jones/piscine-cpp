//
// 42 header
//

#ifndef ENEMY_HPP
# define ENEMY_HPP

# include <iostream>

class Enemy {
public:
	Enemy(int hp, std::string const & type);
	Enemy(const Enemy &copy);
	virtual ~Enemy();
	Enemy				&operator=(const Enemy &assign);
	const std::string	getType() const;
	int					getHP() const;
	virtual void		takeDamage(int amount);
protected:
	int 		m_hp;
	std::string	m_type;
private:
	Enemy();
};

#endif