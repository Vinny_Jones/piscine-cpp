//
// 42 header
//

#include "SuperMutant.hpp"

SuperMutant::SuperMutant() : Enemy(170, "Super Mutant") {
	std::cout << "Gaaah. Me want smash heads !" << std::endl;
}

SuperMutant::~SuperMutant() {
	std::cout << "Aaargh ..." << std::endl;
}

void	SuperMutant::takeDamage(int amount) {
	if (amount <= 3)
		return;
	m_hp = (amount - 3 < m_hp) ? m_hp - (amount - 3) : 0;
}
