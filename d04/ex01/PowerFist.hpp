//
// Created by Andrii Pyvovar on 24.12.17.
//

#ifndef POWERFIST_HPP
# define POWERFIST_HPP

# include "AWeapon.hpp"

class PowerFist : public AWeapon {
public:
	PowerFist();
	// Coplien form ???
	virtual void	attack() const;
};

#endif