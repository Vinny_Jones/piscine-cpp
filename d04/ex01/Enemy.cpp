//
// 42 header
//

#include "Enemy.hpp"

Enemy::Enemy() {
	/* private constructor */
}

Enemy::Enemy(int hp, std::string const &type) : m_hp(hp), m_type(type) {

}

Enemy::Enemy(const Enemy &copy) {
	*this = copy;
}

Enemy::~Enemy() {

}

Enemy&	Enemy::operator=(const Enemy &assign) {
	m_hp = assign.m_hp;
	m_type = assign.m_type;
	return (*this);
}

int		Enemy::getHP() const {
	return (m_hp);
}

const std::string	Enemy::getType() const {
	return (m_type);
}

void	Enemy::takeDamage(int amount) {
	if (amount < 0)
		return;
	m_hp = (amount < m_hp) ? m_hp - amount : 0;
}