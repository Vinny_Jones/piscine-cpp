//
// 42 header
//

#ifndef PLASMARIFLE_HPP
# define PLASMARIFLE_HPP

# include "AWeapon.hpp"

class PlasmaRifle : public AWeapon {
public:
	PlasmaRifle();
	// Coplien form ???
	virtual void	attack() const;
};

#endif
