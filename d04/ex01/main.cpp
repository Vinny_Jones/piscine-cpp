//
// 42 header
//

#include "PlasmaRifle.hpp"
#include "PowerFist.hpp"
#include "SuperMutant.hpp"
#include "RadScorpion.hpp"
#include "Character.hpp"

void	runRest() {
	/*
	 * AWeapon	a("hello", 3, 33);		//abstract class - will not compile if uncomment
	 */

	Character* zaz = new Character("zaz");

	std::cout << *zaz;

	Enemy* b = new RadScorpion();
	Enemy* c = new SuperMutant();

	AWeapon* pr = new PlasmaRifle();
	AWeapon* pf = new PowerFist();

	std::cout << *zaz;
	zaz->attack(b);
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->equip(pf);

	zaz->attack(b);
	std::cout << *zaz;
	std::cout << "Scorp health " << b->getHP() << std::endl;
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->attack(b);
	std::cout << *zaz;
	zaz->attack(b);		/* b deleted here */
	std::cout << "Scorp health " << b->getHP() << std::endl;
	std::cout << *zaz;
	zaz->recoverAP();
	std::cout << *zaz;
	zaz->attack(c);
	std::cout << *zaz;
	zaz->attack(c);
	std::cout << *zaz;
	zaz->attack(c);
	std::cout << "Mutant health " << c->getHP() << std::endl;
	zaz->equip(pf);
	zaz->attack(c);
	zaz->attack(c);
	zaz->attack(c);
	std::cout << "Mutant health " << c->getHP() << std::endl;
	zaz->recoverAP();
	zaz->attack(c);		/* c deleted here  */
	std::cout << *zaz;

	delete pr;
	delete pf;
	delete zaz;
}

int	main() {
	runRest();
	return (0);
}