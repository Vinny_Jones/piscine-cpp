//
// 42 header
//

#include "AWeapon.hpp"

AWeapon::AWeapon() {
	/* private constructor */
}

AWeapon::AWeapon(const AWeapon &copy) {
	*this = copy;
}

AWeapon::AWeapon(std::string const &name, int apcost, int damage)
		: m_name(name), m_apCost(apcost), m_damage(damage)
{
}

AWeapon::~AWeapon() {

}

AWeapon& AWeapon::operator=(const AWeapon &assign) {
	m_name = assign.getName();
	m_apCost = assign.getAPCost();
	m_damage = assign.getDamage();
	return (*this);
}

const std::string	AWeapon::getName() const {
	return (m_name);
}

int		AWeapon::getAPCost() const {
	return (m_apCost);
}

int		AWeapon::getDamage() const {
	return (m_damage);
}
