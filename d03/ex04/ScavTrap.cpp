/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 12:03:56 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 12:03:56 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"

const char *ScavTrap::challenges[] = {
		"Ice bucket Challenge",
		"The Eat It or Wear It Challenge",
		"Try Not to Laugh Challenge",
		"Speed Drawing Challenge",
		"Touch My Body Challenge",
		"Chubby Bunny Challenge",
		"Innuendo Bingo Challenge",
		"The Cinnamon Challenge",
		"Bean Boozled Challenge",
		"Ghost Pepper Challenge"
};

ScavTrap::ScavTrap() {
	m_name = "unknown hero";
	std::cout << "[" << m_name << "] I AM ON FIRE!!! OH GOD, PUT ME OUT!!!" << std::endl;
	setDefault();
}

ScavTrap::ScavTrap(const std::string &name) : ClapTrap(name) {
	m_name = name;
	std::cout<< "[" << m_name << "] Kill, reload! Kill, reload! KILL! RELOAD!" << std::endl;
	setDefault();
}

ScavTrap::ScavTrap(const ScavTrap &copy) : ClapTrap(copy) {
	copyParams(copy);
	std::cout << "[" << m_name << "] Copied!" << std::endl;
}

ScavTrap::~ScavTrap() {
	std::cout << "[" << m_name << "] Good night, lads!" << std::endl;
}

ScavTrap	&ScavTrap::operator=(const ScavTrap &assign) {
	copyParams(assign);
	return (*this);
}

unsigned int ScavTrap::rangedAttack(std::string const &target) {
	std::cout << "Throwing grenade! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at range, causing " << m_range << " points of damage !" << std::endl;
	return (m_range);
}

unsigned int ScavTrap::meleeAttack(std::string const &target) {
	std::cout << "Nade out! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at melee, causing " << m_melee << " points of damage !" << std::endl;
	return (m_melee);
}

void		ScavTrap::challengeNewcomer(const std::string &name) {
	srand(time(NULL));
	std::cout << "Challenge for newcomer " << name << " is going to be... ";
	std::cout << challenges[rand() % 10] << std::endl;
}

void		ScavTrap::setDefault() {
	m_HP = 100;
	m_maxHP = 100;
	m_EP = 50;
	m_maxEP = 50;
	m_level = 1;
	m_melee = 20;
	m_range = 15;
	m_armor = 3;
}
