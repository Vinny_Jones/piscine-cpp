/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 12:03:32 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 12:03:47 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NINJATRAP_HPP
# define NINJATRAP_HPP

# include "ClapTrap.hpp"
# include "FragTrap.hpp"
# include "ScavTrap.hpp"

class NinjaTrap : public virtual ClapTrap {
public:
	NinjaTrap();
	NinjaTrap(const std::string &name);
	NinjaTrap(const NinjaTrap &copy);
	virtual ~NinjaTrap();
	NinjaTrap		&operator=(const NinjaTrap &assign);

	unsigned int 	rangedAttack(std::string const &target);
	unsigned int	meleeAttack(std::string const &target);
	unsigned int	ninjaShoebox(const NinjaTrap &target);
	unsigned int	ninjaShoebox(const FragTrap &target);
	unsigned int	ninjaShoebox(const ScavTrap &target);

private:
	void 	setDefault();
};

#endif
