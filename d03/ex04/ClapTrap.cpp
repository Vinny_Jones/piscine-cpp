/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 12:02:15 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 12:02:15 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"

ClapTrap::ClapTrap() : m_name("ClapTrap") {
	std::cout << "[" << m_name << "] I'm so sexy!" << std::endl;
}

ClapTrap::ClapTrap(const std::string &name) : m_name(name) {
	std::cout << "Now [" << m_name << "] will dominate!" << std::endl;
}

ClapTrap::ClapTrap(const ClapTrap &copy) {
	copyParams(copy);
	std::cout << "[" << m_name << "] ClapTrap...Copying successful! " << std::endl;
}

ClapTrap::~ClapTrap() {
	std::cout << "[" << m_name << "] Time to sleep!" << std::endl;
}

ClapTrap	&ClapTrap::operator=(const ClapTrap &assign) {
	copyParams(assign);
	return (*this);
}

const std::string ClapTrap::name() const {
	return (m_name);
}

unsigned int ClapTrap::HP() {
	return (m_HP);
}

void		ClapTrap::setHP(unsigned int val) {
	m_HP = (val > m_maxHP) ? m_maxHP : val;
}

unsigned int ClapTrap::EP() {
	return (m_EP);
}

void 		ClapTrap::setEP(unsigned int val) {
	m_EP = (val > m_maxEP) ? m_maxEP : val;
}

void		ClapTrap::takeDamage(unsigned int amount) {
	if (!amount)
		return ;
	if (amount > m_armor) {
		unsigned int	clearDamage = amount - m_armor;
		setHP(HP() > clearDamage ? HP() - clearDamage : 0);
	}
	std::cout << "Ouch! " << m_name << " FR4G-TP took " << amount;
	std::cout << " of damage with " << m_armor << " points of armor. HP = " << HP() << "... ";
	std::cout << (HP() ? "holding on!" : "WASTED!!!") << std::endl;
}

void		ClapTrap::beRepaired(unsigned int amount) {
	setHP(HP() + amount);
	std::cout << "Health over here! " << m_name << " FR4G-TP repaired for ";
	std::cout  << amount << " of HP. Like a new one!" << std::endl;
}

void		ClapTrap::copyParams(const ClapTrap &copy) {
	m_HP = copy.m_HP;
	m_maxHP = copy.m_maxHP;
	m_EP = copy.m_EP;
	m_maxEP = copy.m_maxEP;
	m_level = copy.m_level;
	m_name = copy.m_name;
	m_melee = copy.m_melee;
	m_range = copy.m_range;
	m_armor = copy.m_armor;
}
