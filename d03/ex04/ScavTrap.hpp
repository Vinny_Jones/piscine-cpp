/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 12:04:00 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 12:04:12 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

# include "ClapTrap.hpp"

class ScavTrap : public ClapTrap {
public:
	ScavTrap();
	ScavTrap(const std::string &name);
	ScavTrap(const ScavTrap &copy);
	virtual ~ScavTrap();

	ScavTrap		&operator=(const ScavTrap &assign);
	unsigned int 	rangedAttack(std::string const &target);
	unsigned int	meleeAttack(std::string const &target);
	void			challengeNewcomer(const std::string &name);
	
protected:
	static const char	*challenges[];
	void 				setDefault();
};

#endif
