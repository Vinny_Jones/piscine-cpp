/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 12:00:55 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 12:00:55 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

int 	main() {
	FragTrap	a("Wonderful");
	ScavTrap	b("R2D2");
	NinjaTrap	c("Napoleon");
	SuperTrap	super("GOD OF WAR");
	SuperTrap	super2(super);

	std::cout << "=========== Battle" << std::endl;

	a.takeDamage(super.meleeAttack(a.name()));
	b.takeDamage(super.rangedAttack(b.name()));
	b.takeDamage(super.vaulthunter_dot_exe(b.name()));
	a.takeDamage(super.ninjaShoebox(a));
	super2.takeDamage(super.vaulthunter_dot_exe(super2.name()));

	std::cout << "===========" << std::endl;
	return (0);
}
