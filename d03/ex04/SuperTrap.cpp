/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 12:04:17 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 12:09:15 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperTrap.hpp"

SuperTrap::SuperTrap() {
	m_name = "unknown hero";
	std::cout << "SuperTrap ()" << std::endl;
	setDefault();
}

SuperTrap::SuperTrap(const std::string &name) {
	m_name = name;
	std::cout << "SuperTrap " << m_name <<" is here!!!" << std::endl;
	setDefault();
}

SuperTrap::SuperTrap(const SuperTrap &copy)
	: ClapTrap(copy), FragTrap(copy), NinjaTrap(copy)
{
	copyParams(copy);
	std::cout << "SuperTrap " << m_name << " copied. Double power is here!!!" << std::endl;
}

SuperTrap::~SuperTrap() {
	std::cout << "SuperTrap " << m_name << " is destroyed... How is it possible?" << std::endl;
}

SuperTrap	&SuperTrap::operator=(const SuperTrap &assign) {
	copyParams(assign);
	return (*this);
}

void		SuperTrap::setDefault() {
	m_HP = 100;
	m_maxHP = 100;
	m_EP = 120;
	m_maxEP = 120;
	m_level = 1;
	m_melee = 60;
	m_range = 20;
	m_armor = 5;
}
