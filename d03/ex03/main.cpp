/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:55:41 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:55:41 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"

int 	main() {
	FragTrap	a("Wonderful");
	ScavTrap	b("R2D2");
	NinjaTrap	c("Napoleon");
	NinjaTrap	d("???");
	NinjaTrap	e(d);

	std::cout << "=========== Battle" << std::endl;

	c.takeDamage(a.rangedAttack(c.name()));
	e.takeDamage(c.rangedAttack(e.name()));
	e.takeDamage(c.meleeAttack(e.name()));
	a.takeDamage(c.ninjaShoebox(a));
	b.takeDamage(c.ninjaShoebox(b));
	d.takeDamage(c.ninjaShoebox(d));

	std::cout << "===========" << std::endl;
	return (0);
}
