/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:58:37 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:58:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NINJATRAP_HPP
# define NINJATRAP_HPP

# include "ClapTrap.hpp"
# include "FragTrap.hpp"
# include "ScavTrap.hpp"

class NinjaTrap : public ClapTrap {
public:
	NinjaTrap();
	NinjaTrap(const std::string &name);
	NinjaTrap(const NinjaTrap &copy);
	virtual ~NinjaTrap();
	NinjaTrap		&operator=(const NinjaTrap &assign);

	unsigned int 	rangedAttack(std::string const &target) const;
	unsigned int	meleeAttack(std::string const &target) const;
	unsigned int	ninjaShoebox(const NinjaTrap &target) const;
	unsigned int	ninjaShoebox(const FragTrap &target) const;
	unsigned int	ninjaShoebox(const ScavTrap &target) const;
	
private:
	void 			setDefault();
};

#endif
