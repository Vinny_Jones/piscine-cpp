/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:57:37 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:58:44 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap() {
	m_name = "unknown hero";
	std::cout << "[" << m_name << "] Watch out everybody! " << std::endl;
	setDefault();
}

NinjaTrap::NinjaTrap(const std::string &name) : ClapTrap(name) {
	m_name = name;
	std::cout<< "[" << m_name << "] Rebirth of Chuck Norris! " << std::endl;
	setDefault();
}

NinjaTrap::NinjaTrap(const NinjaTrap &copy) : ClapTrap(copy) {
	copyParams(copy);
	std::cout << "[" << m_name << "] Copy..py..py..thon!" << std::endl;
}

NinjaTrap::~NinjaTrap() {
	std::cout << "[" << m_name << "] Geting the Hell out of Here!" << std::endl;
}

NinjaTrap	&NinjaTrap::operator=(const NinjaTrap &assign) {
	copyParams(assign);
	return (*this);
}

unsigned int NinjaTrap::rangedAttack(std::string const &target) const {
	std::cout << "Look out! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at range, causing " << m_range << " points of damage !" << std::endl;
	return (m_range);
}

unsigned int NinjaTrap::meleeAttack(std::string const &target) const {
	std::cout << "Yahooooo! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at melee, causing " << m_melee << " points of damage !" << std::endl;
	return (m_melee);
}

unsigned int	NinjaTrap::ninjaShoebox(const NinjaTrap &target) const {
	std::cout << "Hittin' NinjaTrap " << target.name();
	std::cout << ". Flash fireworks!" << std::endl;
	return (m_melee * 1.4);
}

unsigned int	NinjaTrap::ninjaShoebox(const FragTrap &target) const {
	std::cout << "Hittin' FragTrap " << target.name();
	std::cout << ". Eww! Cool." << std::endl;
	return (m_melee * 1.45);
}

unsigned int	NinjaTrap::ninjaShoebox(const ScavTrap &target) const {
	std::cout << "Hittin' ScavTrap " << target.name();
	std::cout << ". Is that what you look like inside?" << std::endl;
	return (m_melee * 1.5);
}

void		NinjaTrap::setDefault() {
	m_HP = 60;
	m_maxHP = 60;
	m_EP = 120;
	m_maxEP = 120;
	m_level = 1;
	m_melee = 60;
	m_range = 5;
	m_armor = 0;
}
