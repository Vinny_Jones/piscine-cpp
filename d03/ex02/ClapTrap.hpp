/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:54:11 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:54:18 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

# include <iostream>
# include <cstdlib>

class ClapTrap {
public:
	ClapTrap();
	ClapTrap(const std::string &name);
	ClapTrap(const ClapTrap &copy);
	virtual ~ClapTrap();
	ClapTrap			&operator=(const ClapTrap &assign);

	const std::string	name(void) const ;
	unsigned int		HP(void) const;
	void 				setHP(unsigned int val);
	unsigned int		EP(void) const;
	void 				setEP(unsigned int val);
	void				takeDamage(unsigned int amount);
	void				beRepaired(unsigned int amount);
	
protected:
	void			copyParams(const ClapTrap &copy);
	unsigned int	m_HP;
	unsigned int	m_maxHP;
	unsigned int	m_EP;
	unsigned int	m_maxEP;
	unsigned int	m_level;
	std::string		m_name;
	unsigned int	m_melee;
	unsigned int	m_range;
	unsigned int	m_armor;
};


#endif
