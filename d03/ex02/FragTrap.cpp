/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:54:36 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:54:36 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

const char *FragTrap::vaulthunterAttacks[] = {
		"This time it'll be awesome, I promise!!!",
		"Hey everybody, check out my package!!!",
		"Place your bets!!!",
		"It's happening... it's happening!!!",
		"It's about to get magical!!!",
		"I'm pulling tricks outta my hat!!!",
		"You can't just program this level of excitement!!!",
		"Things are about to get awesome!!!",
		"Round and around and around she goes!!!",
		"It's like a box of chocolates..."
	};

FragTrap::FragTrap() {
	m_name = "unknown hero";
	std::cout << "[" << m_name << "] Let's get this party started!" << std::endl;
	setDefault();
}

FragTrap::FragTrap(const std::string &name) : ClapTrap(name) {
	m_name = name;
	std::cout << "[" << m_name << "] Glitching weirdness is a term of endearment, right?" << std::endl;
	setDefault();
}

FragTrap::FragTrap(const FragTrap &copy) : ClapTrap(copy) {
	copyParams(copy);
	std::cout << "Copied! Who said plagiarism is bad?" << std::endl;
}

FragTrap::~FragTrap() {
	std::cout << "[" << m_name << "] See ya, guys!" << std::endl;
}

FragTrap	&FragTrap::operator=(const FragTrap &assign) {
	copyParams(assign);
	return (*this);
}

unsigned int FragTrap::rangedAttack(std::string const &target) const {
	std::cout << "Present for you! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at range, causing " << m_range << " points of damage !" << std::endl;
	return (m_range);
}

unsigned int FragTrap::meleeAttack(std::string const &target) const {
	std::cout << "Take that! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at melee, causing " << m_melee << " points of damage !" << std::endl;
	return (m_melee);
}

unsigned int FragTrap::vaulthunter_dot_exe(std::string const &target) {
	unsigned int damageAmount = 0;

	if (EP() >= 25) {
		setEP(EP() - 25);
		srand(time(NULL));
		std::cout << vaulthunterAttacks[rand() % 10] << std::endl;
		for (int i = 0; i < 3; ++i) {
			srand(time(NULL) + i);
			damageAmount += ((rand() % 2) ? rangedAttack(target) : meleeAttack(target));
		}
	} else {
		std::cout << "Darn! " << m_name << " out of energy! ";
		std::cout << target << " lucky bastard!!!" << std::endl;
	}
	return (damageAmount);
}

void		FragTrap::setDefault() {
	m_HP = 100;
	m_maxHP = 100;
	m_EP = 100;
	m_maxEP = 100;
	m_level = 1;
	m_melee = 30;
	m_range = 20;
	m_armor = 5;
}
