/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:54:41 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:54:51 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap : public ClapTrap {
public:
	FragTrap();
	FragTrap(const std::string &name);
	FragTrap(const FragTrap &copy);
	virtual ~FragTrap();

	FragTrap		&operator=(const FragTrap &assign);
	unsigned int 	rangedAttack(std::string const &target) const;
	unsigned int	meleeAttack(std::string const &target) const;
	unsigned int	vaulthunter_dot_exe(std::string const &target);
	
private:
	static const char *vaulthunterAttacks[];
	void 			setDefault();
};

#endif
