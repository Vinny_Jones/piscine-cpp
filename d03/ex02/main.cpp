/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:53:38 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:53:39 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int 	main() {
	FragTrap	a;
	FragTrap	b("Wonderful");
	ScavTrap	c("R2D2");

//	a = c; /* error must be here. not assignable */
	a = b;

	std::cout << "=========== Battle" << std::endl;

	c.takeDamage(a.rangedAttack(c.name()));
	a.takeDamage(b.rangedAttack(a.name()));
	a.takeDamage(c.meleeAttack(a.name()));
	c.takeDamage(b.vaulthunter_dot_exe(c.name()));
	c.challengeNewcomer(b.name());
	b.takeDamage(c.rangedAttack(b.name()));

	std::cout << "===========" << std::endl;
	return (0);
}
