/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:45:32 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:45:49 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

# include <iostream>
# include <cstdlib>

class FragTrap {
public:
	FragTrap();
	FragTrap(const std::string &name);
	FragTrap(const FragTrap &copy);
	virtual ~FragTrap();

	FragTrap			&operator=(const FragTrap &assign);

	const std::string	name(void) const;
	unsigned int		HP(void) const;
	void 				setHP(unsigned int val);
	unsigned int		EP(void) const;
	void 				setEP(unsigned int val);
	unsigned int 		rangedAttack(std::string const &target) const;
	unsigned int		meleeAttack(std::string const &target) const;
	void				takeDamage(unsigned int amount);
	void				beRepaired(unsigned int amount);
	unsigned int		vaulthunter_dot_exe(std::string const &target);
private:
	static const char *vaulthunterAttacks[];
	void 			setDefault();
	unsigned int	m_HP;
	unsigned int	m_maxHP;
	unsigned int	m_EP;
	unsigned int	m_maxEP;
	unsigned int	m_level;
	std::string		m_name;
	unsigned int	m_melee;
	unsigned int	m_range;
	unsigned int	m_armor;
};

#endif
