/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:48:38 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:48:41 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

int 	main() {
	FragTrap	a;
	FragTrap	b("R2D2");
	FragTrap	*c = new FragTrap(b);
	FragTrap	d("Wonderful");

	delete c;

	std::cout << "=========== Battle" << std::endl;

	b.takeDamage(a.rangedAttack(b.name()));
	a.takeDamage(b.rangedAttack(a.name()));
	a.takeDamage(b.meleeAttack(a.name()));
	a.takeDamage(b.vaulthunter_dot_exe(a.name()));

	FragTrap	*dd = new FragTrap(d);

	d.takeDamage(b.vaulthunter_dot_exe(d.name()));
	dd->takeDamage(b.vaulthunter_dot_exe(dd->name()));
	dd->takeDamage(b.vaulthunter_dot_exe(dd->name()));
	d.takeDamage(b.vaulthunter_dot_exe(d.name()));

	dd->beRepaired(120);
	std::cout << dd->name() << " health is repaired to " << dd->HP() << std::endl;

	std::cout << "===========" << std::endl;

	delete dd;
	return (0);
}
