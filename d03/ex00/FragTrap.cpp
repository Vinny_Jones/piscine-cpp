/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:45:06 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:45:09 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

const char *FragTrap::vaulthunterAttacks[] = {
		"This time it'll be awesome, I promise!!!",
		"Hey everybody, check out my package!!!",
		"Place your bets!!!",
		"It's happening... it's happening!!!",
		"It's about to get magical!!!",
		"I'm pulling tricks outta my hat!!!",
		"You can't just program this level of excitement!!!",
		"Things are about to get awesome!!!",
		"Round and around and around she goes!!!",
		"It's like a box of chocolates..."
	};

FragTrap::FragTrap() : m_name("[unknown hero]") {
	std::cout << "Let's get this party started!" << std::endl;
	setDefault();
}

FragTrap::FragTrap(const std::string &name) : m_name(name) {
	std::cout << "Glitching weirdness is a term of endearment, right?" << std::endl;
	setDefault();
}

FragTrap::FragTrap(const FragTrap &copy) {
	std::cout << "Copied! Who said plagiarism is bad?" << std::endl;
	m_HP = copy.m_HP;
	m_maxHP = copy.m_maxHP;
	m_EP = copy.m_EP;
	m_maxEP = copy.m_maxEP;
	m_level = copy.m_level;
	m_name = copy.m_name;
	m_melee = copy.m_melee;
	m_range = copy.m_range;
	m_armor = copy.m_armor;
}

FragTrap::~FragTrap() {
	std::cout << m_name << ": See ya, guys!" << std::endl;
}

FragTrap	&FragTrap::operator=(const FragTrap &assign) {
	m_HP = assign.m_HP;
	m_maxHP = assign.m_maxHP;
	m_EP = assign.m_EP;
	m_maxEP = assign.m_maxEP;
	m_level = assign.m_level;
	m_name = assign.m_name;
	m_melee = assign.m_melee;
	m_range = assign.m_range;
	m_armor = assign.m_armor;
	return (*this);
}

const std::string FragTrap::name() const {
	return (m_name);
}

unsigned int FragTrap::HP() const {
	return (m_HP);
}

void		FragTrap::setHP(unsigned int val) {
	m_HP = (val > m_maxHP) ? m_maxHP : val;
}

unsigned int FragTrap::EP() const {
	return (m_EP);
}

void 		FragTrap::setEP(unsigned int val) {
	m_EP = (val > m_maxEP) ? m_maxEP : val;
}

unsigned int FragTrap::rangedAttack(std::string const &target) const {
	std::cout << "Present for you! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at range, causing " << m_range << " points of damage !" << std::endl;
	return (m_range);
}

unsigned int FragTrap::meleeAttack(std::string const &target) const {
	std::cout << "Take that! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at melee, causing " << m_melee << " points of damage !" << std::endl;
	return (m_melee);
}

void		FragTrap::takeDamage(unsigned int amount) {
	if (!amount)
		return ;
	if (amount > m_armor) {
		unsigned int	clearDamage = amount - m_armor;
		setHP(HP() > clearDamage ? HP() - clearDamage : 0);
	}
	std::cout << "Ouch! " << m_name << " FR4G-TP took " << amount;
	std::cout << " of damage with " << m_armor << " points of armor. HP = " << HP() << "... ";
	std::cout << (HP() ? "holding on!" : "WASTED!!!") << std::endl;
}

void		FragTrap::beRepaired(unsigned int amount) {
	setHP(HP() + amount);
	std::cout << "Health over here! " << m_name << " FR4G-TP repaired for ";
	std::cout  << amount << " of HP. Like a new one!" << std::endl;
}

unsigned int FragTrap::vaulthunter_dot_exe(std::string const &target) {
	unsigned int damageAmount = 0;

	if (EP() >= 25) {
		setEP(EP() - 25);
		srand(time(NULL));
		std::cout << vaulthunterAttacks[rand() % 10] << std::endl;
		for (int i = 0; i < 3; ++i) {
			srand(time(NULL) + i);
			damageAmount += ((rand() % 2) ? rangedAttack(target) : meleeAttack(target));
		}
	} else {
		std::cout << "Darn! " << m_name << " out of energy! ";
		std::cout << target << " lucky bastard!!!" << std::endl;
	}
	return (damageAmount);
}

void		FragTrap::setDefault() {
	m_HP = 100;
	m_maxHP = 100;
	m_EP = 100;
	m_maxEP = 100;
	m_level = 1;
	m_melee = 30;
	m_range = 20;
	m_armor = 5;
}
