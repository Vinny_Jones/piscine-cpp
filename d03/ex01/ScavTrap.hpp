/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:51:36 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:51:53 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

# include <iostream>
# include <cstdlib>

class ScavTrap {
public:
	ScavTrap();
	ScavTrap(const std::string &name);
	ScavTrap(const ScavTrap &copy);
	virtual ~ScavTrap();
	ScavTrap			&operator=(const ScavTrap &assign);

	const std::string	name(void) const;
	unsigned int		HP(void) const;
	void 				setHP(unsigned int val);
	unsigned int		EP(void) const;
	void 				setEP(unsigned int val);
	unsigned int 		rangedAttack(std::string const &target) const;
	unsigned int		meleeAttack(std::string const &target) const;
	void				takeDamage(unsigned int amount);
	void				beRepaired(unsigned int amount);
	void				challengeNewcomer(const std::string &name);
	
private:
	static const char *challenges[];
	void 			setDefault();
	unsigned int	m_HP;
	unsigned int	m_maxHP;
	unsigned int	m_EP;
	unsigned int	m_maxEP;
	unsigned int	m_level;
	std::string		m_name;
	unsigned int	m_melee;
	unsigned int	m_range;
	unsigned int	m_armor;
};


#endif
