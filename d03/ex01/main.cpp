/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:50:31 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:52:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int 	main() {
	FragTrap	a;
	FragTrap	b("R2D2");
	FragTrap	*c = new FragTrap(b);
	FragTrap	d("Wonderful");

	delete c;

	std::cout << "=========== FragTrap Battle" << std::endl;

	b.takeDamage(a.rangedAttack(b.name()));
	a.takeDamage(b.rangedAttack(a.name()));
	a.takeDamage(b.meleeAttack(a.name()));
	a.takeDamage(b.vaulthunter_dot_exe(a.name()));

	FragTrap	*dd = new FragTrap(d);

	d.takeDamage(b.vaulthunter_dot_exe(d.name()));
	dd->takeDamage(b.vaulthunter_dot_exe(dd->name()));
	dd->takeDamage(b.vaulthunter_dot_exe(dd->name()));
	d.takeDamage(b.vaulthunter_dot_exe(d.name()));

	dd->beRepaired(120);
	std::cout << dd->name() << " health is repaired to " << dd->HP() << std::endl;

	delete dd;
	std::cout << "===========" << std::endl;

	ScavTrap	aS;
	ScavTrap	bS("Inglorious bastard");
	ScavTrap	*cS = new ScavTrap(bS);
	ScavTrap	dS("Wonderful");

	std::cout << "=========== ScavTrap Battle" << std::endl;

	bS.takeDamage(aS.rangedAttack(bS.name()));
	aS.takeDamage(bS.rangedAttack(aS.name()));
	aS.takeDamage(bS.meleeAttack(aS.name()));
	aS.takeDamage(b.meleeAttack(aS.name()));
	aS.challengeNewcomer(cS->name());


	aS.beRepaired(120);
	std::cout << aS.name() << " health is repaired to " << aS.HP() << std::endl;

	std::cout << "===========" << std::endl;

	delete cS;
	return (0);
}
