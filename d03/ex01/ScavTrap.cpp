/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:52:00 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/18 11:52:02 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"


const char *ScavTrap::challenges[] = {
		"Ice bucket Challenge",
		"The Eat It or Wear It Challenge",
		"Try Not to Laugh Challenge",
		"Speed Drawing Challenge",
		"Touch My Body Challenge",
		"Chubby Bunny Challenge",
		"Innuendo Bingo Challenge",
		"The Cinnamon Challenge",
		"Bean Boozled Challenge",
		"Ghost Pepper Challenge"
};

ScavTrap::ScavTrap() : m_name("unknown hero")  {
	std::cout << "[" << m_name << "] I AM ON FIRE!!! OH GOD, PUT ME OUT!!!" << std::endl;
	setDefault();
}

ScavTrap::ScavTrap(const std::string &name) : m_name(name)  {
	std::cout<< "[" << m_name << "] Kill, reload! Kill, reload! KILL! RELOAD!" << std::endl;
	setDefault();
}

ScavTrap::ScavTrap(const ScavTrap &copy) {
	m_HP = copy.m_HP;
	m_maxHP = copy.m_maxHP;
	m_EP = copy.m_EP;
	m_maxEP = copy.m_maxEP;
	m_level = copy.m_level;
	m_name = copy.m_name;
	m_melee = copy.m_melee;
	m_range = copy.m_range;
	m_armor = copy.m_armor;
	std::cout << "[" << m_name << "] Copied!" << std::endl;
}

ScavTrap::~ScavTrap() {
	std::cout << "[" << m_name << "] Good night, lads!" << std::endl;
}

ScavTrap	&ScavTrap::operator=(const ScavTrap &assign) {
	m_HP = assign.m_HP;
	m_maxHP = assign.m_maxHP;
	m_EP = assign.m_EP;
	m_maxEP = assign.m_maxEP;
	m_level = assign.m_level;
	m_name = assign.m_name;
	m_melee = assign.m_melee;
	m_range = assign.m_range;
	m_armor = assign.m_armor;
	return (*this);
}

const std::string ScavTrap::name() const {
	return (m_name);
}

unsigned int ScavTrap::HP() const {
	return (m_HP);
}

void		ScavTrap::setHP(unsigned int val) {
	m_HP = (val > m_maxHP) ? m_maxHP : val;
}

unsigned int ScavTrap::EP() const {
	return (m_EP);
}

void 		ScavTrap::setEP(unsigned int val) {
	m_EP = (val > m_maxEP) ? m_maxEP : val;
}

unsigned int ScavTrap::rangedAttack(std::string const &target) const {
	std::cout << "Throwing grenade! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at range, causing " << m_range << " points of damage !" << std::endl;
	return (m_range);
}

unsigned int ScavTrap::meleeAttack(std::string const &target) const {
	std::cout << "Nade out! " << m_name << " FR4G-TP attacks " << target;
	std::cout << " at melee, causing " << m_melee << " points of damage !" << std::endl;
	return (m_melee);
}

void		ScavTrap::takeDamage(unsigned int amount) {
	if (!amount)
		return ;
	if (amount > m_armor) {
		unsigned int	clearDamage = amount - m_armor;
		setHP(HP() > clearDamage ? HP() - clearDamage : 0);
	}

	std::cout << "Ouch! " << m_name << " FR4G-TP took " << amount;
	std::cout << " of damage with " << m_armor << " points of armor. HP = " << HP() << "... ";
	std::cout << (HP() ? "holding on!" : "WASTED!!!") << std::endl;
}

void		ScavTrap::beRepaired(unsigned int amount) {
	setHP(HP() + amount);
	std::cout << "Health over here! " << m_name << " FR4G-TP repaired for ";
	std::cout  << amount << " of HP. Like a new one!" << std::endl;
}

void		ScavTrap::challengeNewcomer(const std::string &name) {
	srand(time(NULL));
	std::cout << "Challenge for newcomer " << name << " is going to be... ";
	std::cout << challenges[rand() % 10] << std::endl;
}

void		ScavTrap::setDefault() {
	m_HP = 100;
	m_maxHP = 100;
	m_EP = 50;
	m_maxEP = 50;
	m_level = 1;
	m_melee = 20;
	m_range = 15;
	m_armor = 3;
}
