//HEADER

#include "span.hpp"

Span::Span(unsigned int n) : m_size(n) {
	m_container.reserve(n);
	m_spanContainer.reserve(n - 1);
}

Span::Span(const Span &rhs) : m_size(rhs.m_size), m_container(rhs.m_container) {}

Span::~Span() {}

Span	&Span::operator=(const Span &rhs) {
	m_size = rhs.m_size;
	m_container = rhs.m_container;
	return (*this);
}

void	Span::addNumber(int nbr) {
	if (m_container.size() == m_size)
		throw std::logic_error("Container filled up");
	m_container.push_back(nbr);
}

unsigned int		Span::shortestSpan() {
	checkContainer();
	createSpanContainer();
	return (std::abs(*std::min_element(m_spanContainer.begin(), m_spanContainer.end())));
}

unsigned int	Span::longestSpan() {
	return (0);

}

void	Span::checkContainer() {
	if (m_container.size() < 2)
		throw std::logic_error("Container size too small for span");
}

void	Span::createSpanContainer() {
	if (m_spanContainer.size() == m_container.size() - 1)
		return;
	std::transform(m_container.begin() + 1, m_container.end(), m_container.begin(), m_spanContainer.begin(), std::minus<int>());
}
