//HEADER

#ifndef SPAN_HPP
# define SPAN_HPP

# include <vector>
# include <iostream>
# include <exception>
# include <algorithm>

class Span {
public:
	Span(unsigned int n = 0);
	Span(const Span &rhs);
	~Span();
	Span	&operator=(const Span &rhs);

	void			addNumber(int nbr);
	unsigned int	shortestSpan();
	unsigned int	longestSpan();

private:
	void				checkContainer();
	void				createSpanContainer();
	unsigned int		m_size;
	std::vector<int>	m_container;
	std::vector<int>	m_spanContainer;
};

#endif
