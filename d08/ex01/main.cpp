//HEADER

#include "span.hpp"

int		main() {
	Span	a(10);

	for (int i = 0; i < 10; i++) {
		a.addNumber(-(i + i * i));
	}

	// for (size_t i = 0; i < a.m_container.size(); i++) {
	// 	std::cout << a.m_container[i] << " ";
	// }
	// std::cout << std::endl;
	std::cout << "Longest span " << a.longestSpan() << std::endl;
	std::cout << "Shortest span " << a.shortestSpan() << std::endl;

	return (0);
}
