//HEADER

#ifndef EASYFIND_HPP
# define EASYFIND_HPP

#include <iterator>
#include <algorithm>

template <typename T>
typename T::iterator	easyfind(T &c, int value) {
	return(std::find(c.begin(), c.end(), value));
};

#endif
