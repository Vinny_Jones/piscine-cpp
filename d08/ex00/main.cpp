#include "easyfind.hpp"
#include <iostream>
#include <vector>

int		main() {
	std::vector<int> v;
	std::vector<int>::iterator it = easyfind(v, 5);

	std::cout << "Result: ";
	if (it == v.end())
	 	std::cout << "Didn't find";
	else
	 	std::cout << *it;
	std::cout << std::endl;

	for (int i = 1; i < 5; i++) {
		v.push_back(i * i);
		v.push_back(i + i + i);
	}

	it = v.begin();
	std::cout << "Vector contents: ";
	while(it != v.end()) {
		std::cout << *it++ << " ";
	}
	std::cout << std::endl;

	std::cout << "Result: ";
	if ((it = easyfind(v, 9)) == v.end())
		std::cout << "Didn't find";
	else
		std::cout << *it;
	std::cout << std::endl;

	std::cout << "Previous element: " << *--it << std::endl;


	return (0);
}
