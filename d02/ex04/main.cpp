//
// 42 header
//

#include "Eval_expr.hpp"

int 	main(int argc, const char *argv[]) {
	if (argc == 2) {
		float result;
		std::stringstream	sstream(argv[1]);
		sstream.setf(std::ios::skipws);
		try {
			result = eval_expr(sstream);
		} catch (std::exception &exc) {
			std::cout << "Error!!! " << exc.what() << std::endl;
			return (0);
		}
		std::cout << "result = " << std::setprecision(3) << result << std::endl;
	}
	else
		std::cout << "One and only one argument is needed" << std::endl;
	return (0);
}