/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Eval_expr.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 10:23:04 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/17 10:23:05 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Eval_expr.hpp"

float 	eval_expr(std::stringstream &sstream) {
	float 	nbr1, nbr2;
	char 	sign1, sign2;

	std::size_t startPos  = sstream.str().find('(');
	if (startPos != std::string::npos) {
		std::size_t endPos = sstream.str().find_last_of(')');
		if (endPos == std::string::npos)
			throw std::logic_error("Brackets syntax error");
		std::stringstream	substr(sstream.str().substr(startPos + 1, endPos - startPos - 1));
		std::stringstream	temp;
		temp << sstream.str().substr(0, startPos);
		temp << eval_expr(substr) << sstream.str().substr(endPos + 1);
		sstream.swap(temp);
		sstream.seekg(0);
	}

	while (!sstream.eof()) {
		if ((sstream >> nbr1).fail())
			throw std::logic_error("Syntax error: Operand error");
		if ((sstream >> sign1).eof())
			return (nbr1);
		if ((sstream >> nbr2).fail())
			throw std::logic_error("Syntax error: Operand error");
		if ((sstream >> sign2).eof())
			return (resolveOperation(nbr1, nbr2, sign1));
		if ((sign1 == '+' || sign1 == '-') && (sign2 == '*' || sign2 == '/')) {
			std::stringstream tempstream;
			tempstream << nbr1 << sign1;
			if ((sstream >> nbr1).fail())
				throw std::logic_error("Syntax error: Operand error");
			tempstream << resolveOperation(nbr2, nbr1, sign2);
			if (!sstream.eof())
				tempstream << sstream.str().substr(sstream.tellg());
			sstream.swap(tempstream);
		} else {
			std::stringstream tempstream;
			tempstream << resolveOperation(nbr1, nbr2, sign1) << sign2;
			if (!sstream.eof())
				tempstream << sstream.str().substr(sstream.tellg());
			sstream.swap(tempstream);
		}
		sstream.seekg(0);
	}
	return (0);
}

float 	resolveOperation(Fixed a, Fixed b, char oper) {
	switch (oper) {
		case ('*'):
			return ((a * b).toFloat());
		case ('/'):
			return ((a / b).toFloat());
		case ('+'):
			return ((a + b).toFloat());
		case ('-'):
			return ((a - b).toFloat());
		default:
			throw std::logic_error("Syntax error: wrong operation");
	}
}
