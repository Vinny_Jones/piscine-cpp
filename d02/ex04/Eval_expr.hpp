/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Eval_expr.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 10:24:52 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/17 10:24:55 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVAL_EXPR_HPP
# define EVAL_EXPR_HPP

# include "Fixed.hpp"
# include <sstream>
# include <iomanip>

float 	eval_expr(std::stringstream &stream);
float 	resolveOperation(Fixed a, Fixed b, char oper);

#endif
