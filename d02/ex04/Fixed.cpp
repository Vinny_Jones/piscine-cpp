/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 10:25:38 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/17 10:25:39 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"

const int	Fixed::bits = 8;

Fixed::Fixed() {
	setRawBits(0);
}

Fixed::Fixed(const int nbr) {
	setRawBits(MAKE_FIXED(nbr, Fixed::bits));
}

Fixed::Fixed(const float nbr) {
	setRawBits(MAKE_FIXED(nbr, Fixed::bits));
}

Fixed::Fixed(const Fixed &rhs) {
	*this = rhs;
}

Fixed::~Fixed() {
}

Fixed	&Fixed::operator=(const Fixed &rhs) {
	setRawBits(rhs.getRawBits());
	return (*this);
}

Fixed	&Fixed::operator+(const Fixed &rhs) {
	setRawBits(getRawBits() + rhs.getRawBits());
	return (*this);
}

Fixed	&Fixed::operator++() {
	setRawBits(getRawBits() + 1);
	return (*this);
}

Fixed	Fixed::operator++(int) {
	Fixed	temp(*this);
	operator++();
	return (temp);
}

Fixed	&Fixed::operator--() {
	setRawBits(getRawBits() - 1);
	return (*this);
}

Fixed	Fixed::operator--(int) {
	Fixed	temp(*this);
	operator--();
	return (temp);
}

Fixed	&Fixed::operator-(const Fixed &rhs) {
	setRawBits(getRawBits() - rhs.getRawBits());
	return (*this);
}

bool	Fixed::operator==(const Fixed &obj) {
	return (getRawBits() == obj.getRawBits());
}

bool	Fixed::operator!=(const Fixed &obj) {
	return (!(*this == obj));
}

bool	Fixed::operator<(const Fixed &obj) {
	return (getRawBits() < obj.getRawBits());
}

bool	Fixed::operator<=(const Fixed &obj) {
	return (*this < obj || *this == obj);
}

Fixed	&Fixed::operator*(const Fixed &rhs) {
	setRawBits((int)roundf(((int64_t)getRawBits() * rhs.getRawBits()) >> Fixed::bits));
	return (*this);
}

Fixed	&Fixed::operator/(const Fixed &rhs) {
	int 	divisionResult = 0;

	if (rhs.getRawBits() == 0)
		throw (std::logic_error("Division by zero"));
	else
		divisionResult = (int)roundf(((int64_t)getRawBits() << Fixed::bits) / rhs.getRawBits());
	setRawBits(divisionResult);
	return (*this);
}

bool	Fixed::operator>(const Fixed &obj) {
	return(!(*this <= obj));
}

bool	Fixed::operator>=(const Fixed &obj) {
	return(!(*this < obj));
}

int		Fixed::getRawBits(void) const {
	return (m_fpv);
}

void	Fixed::setRawBits(int const raw) {
	m_fpv = raw;
}

float	Fixed::toFloat(void) const {
	return (MAKE_FLOAT(m_fpv, Fixed::bits));
}

int		Fixed::toInt(void) const {
	return ((int)roundf(toFloat()));
}

std::ostream	&operator<<(std::ostream &stream, const Fixed &obj) {
	return (stream << obj.toFloat());
}

Fixed		&Fixed::max(Fixed &lhs, Fixed &rhs) {
	return (lhs > rhs ? lhs : rhs);
}

const Fixed	&Fixed::max(const Fixed &lhs, const Fixed &rhs) {
	return (lhs.getRawBits() > rhs.getRawBits() ? lhs : rhs);
}

Fixed		&Fixed::min(Fixed &lhs, Fixed &rhs) {
	return (lhs < rhs ? lhs : rhs);
}

const Fixed	&Fixed::min(const Fixed &lhs, const Fixed &rhs) {
	return (lhs.getRawBits() < rhs.getRawBits() ? lhs : rhs);
}
