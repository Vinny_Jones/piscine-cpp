/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 10:19:23 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/17 10:19:42 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

# include <iostream>
# include <cmath>
# define MAKE_FIXED(A, B) ((int)roundf((A) * (1LL << (B))))
# define MAKE_FLOAT(A, B) ((float)(A) / (1LL << (B)))

class Fixed {
public:
	Fixed();
	Fixed(const int nbr);
	Fixed(const float nbr);
	Fixed(const Fixed &rhs);
	~Fixed();
	Fixed			&operator=(const Fixed &rhs);
	
	int		getRawBits(void) const;
	void	setRawBits(int const raw);
	float	toFloat(void) const;
	int		toInt(void) const;
private:
	int					m_fpv;
	const static int	bits;
};

std::ostream	&operator<<(std::ostream &stream, const Fixed &obj);

#endif
