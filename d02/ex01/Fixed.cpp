/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 10:19:51 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/17 10:19:53 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"

const int	Fixed::bits = 8;

Fixed::Fixed() {
	std::cout << "Default constructor called" << std::endl;
	setRawBits(0);
}

Fixed::Fixed(const int nbr) {
	std::cout << "Int constructor called" << std::endl;
	setRawBits(MAKE_FIXED(nbr, Fixed::bits));
}

Fixed::Fixed(const float nbr) {
	std::cout << "Float constructor called" << std::endl;
	setRawBits(MAKE_FIXED(nbr, Fixed::bits));
}

Fixed::Fixed(const Fixed &rhs) {
	std::cout << "Copy constructor called" << std::endl;
	*this = rhs;
}

Fixed::~Fixed() {
	std::cout << "Destructor called" << std::endl;
}

Fixed	&Fixed::operator=(const Fixed &rhs) {
	std::cout << "Assignation operator called" << std::endl;
	setRawBits(rhs.getRawBits());
	return (*this);
}

int		Fixed::getRawBits(void) const {
	return (m_fpv);
}

void	Fixed::setRawBits(int const raw) {
	m_fpv = raw;
}

float	Fixed::toFloat(void) const {
	return (MAKE_FLOAT(m_fpv, Fixed::bits));
}

int		Fixed::toInt(void) const {
	return (roundf(toFloat()));
}

std::ostream	&operator<<(std::ostream &stream, const Fixed &obj) {
	return (stream << obj.toFloat());
}
