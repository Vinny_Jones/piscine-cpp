/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 10:20:58 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/17 10:26:47 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

# include <iostream>
# include <cmath>
# define MAKE_FIXED(A, B) ((int)roundf((A) * (1LL << (B))))
# define MAKE_FLOAT(A, B) ((float)(A) / (1LL << (B)))

class Fixed {
public:
	static Fixed		&max(Fixed &lhs, Fixed &rhs);
	const static Fixed	&max(const Fixed &lhs, const Fixed &rhs);
	static Fixed		&min(Fixed &lhs, Fixed &rhs);
	const static Fixed	&min(const Fixed &lhs, const Fixed &rhs);

	Fixed();
	Fixed(const int nbr);
	Fixed(const float nbr);
	Fixed(const Fixed &rhs);
	~Fixed();

	Fixed	&operator=(const Fixed &rhs);
	Fixed	&operator+(const Fixed &rhs);
	Fixed	&operator-(const Fixed &rhs);
	Fixed	&operator*(const Fixed &rhs);
	Fixed	&operator/(const Fixed &rhs);
	Fixed	&operator++();
	Fixed	operator++(int);
	Fixed	&operator--();
	Fixed	operator--(int);
	bool	operator==(const Fixed &obj);
	bool	operator!=(const Fixed &obj);
	bool	operator<(const Fixed &obj);
	bool	operator<=(const Fixed &obj);
	bool	operator>(const Fixed &obj);
	bool	operator>=(const Fixed &obj);

	int		getRawBits(void) const;
	void	setRawBits(int const raw);
	float	toFloat(void) const;
	int		toInt(void) const;

private:
	int					m_fpv;
	const static int	bits;
};

std::ostream	&operator<<(std::ostream &stream, const Fixed &obj);

#endif
