/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 10:18:50 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/17 10:19:04 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

# include <iostream>

class Fixed {
public:
	Fixed();
	Fixed(const Fixed &rhs);
	~Fixed();
	Fixed	&operator=(const Fixed &rhs);
	
	int		getRawBits(void) const;
	void	setRawBits(int const raw);
private:
	int					m_fpv;
	const static int	bits;
};

#endif
