/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:14:44 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:14:51 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"

int ZombieEvent::counter = 0;

ZombieEvent::ZombieEvent(const std::string &type) {
	setZombieType(type);
}

ZombieEvent::ZombieEvent(const ZombieEvent &copy) : Zombie(copy) {}

ZombieEvent::~ZombieEvent() {}

ZombieEvent	&ZombieEvent::operator=(const ZombieEvent &assign) {
	Zombie::operator=(assign);
	return (*this);
}

void	ZombieEvent::setZombieType(const std::string &type) {
	m_type = type;
}

Zombie*	ZombieEvent::newZombie(std::string name) {
	m_name = name;
	ZombieEvent::counter++;
	return (new Zombie(*this));
}

Zombie*	ZombieEvent::randomChump() {
	srand (time(NULL) + ZombieEvent::counter);
	Zombie*		newGuy = newZombie(Zombie::Names[rand() % 25]);
	newGuy->announce();
	return (newGuy);
}
