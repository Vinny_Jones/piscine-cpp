/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:14:13 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:14:19 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

const std::string Zombie::Names[25] = {
   "Brainy Allegheny", "Undead Jack Seizmore", "Bone Toothpick Juanita",
   "Missing Mandable Melvin", "Hiro the Gutless", "Rott Wilder",
   "‘Twas the Night Before Clement", "Club Foot", "No Feet",
   "Jasmine Boneforhands", "One Eyed William", "Mall Roaming Kevin Eubanks",
   "Mmrgh", "Mort A. Mortis", "Slim James",
   "No Tongue Hampton", "The Silent Partner", "It’s not easy being Ed Greene",
   "Damn Flanders", "Cavity Chompsky", "Richard the Missing Hearted",
   "Carol Channing", "Fanger", "Foos", "Meredith the Deaf" };

Zombie::Zombie(std::string name, std::string type) : m_type(type), m_name(name) {}

Zombie::~Zombie() {}

Zombie::Zombie(const Zombie &copy) {
	*this = copy;
}

Zombie	&Zombie::operator=(const Zombie &assign) {
	m_name = assign.m_name;
	m_type = assign.m_type;
	return (*this);
}

void	Zombie::announce() const {
	std::cout << "<" << m_name << " (" << m_type << ")> Braiiiiiiinnnssss..." << std::endl;
}
