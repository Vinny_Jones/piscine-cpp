/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:14:33 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:14:35 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_HPP
# define ZOMBIEEVENT_HPP

#include "Zombie.hpp"
#include <cstdlib>

class ZombieEvent : public Zombie {
	static int counter;
public:
	ZombieEvent(const std::string &type = "walker");
	ZombieEvent(const ZombieEvent &copy);
	virtual ~ZombieEvent();
	ZombieEvent	&operator=(const ZombieEvent &assign);

	void	setZombieType(const std::string &type);
	Zombie*	newZombie(std::string name);
	Zombie*	randomChump();
};

#endif
