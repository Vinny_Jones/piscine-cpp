/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:13:24 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:13:37 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"

int		main() {
	Zombie		zombieAlone("Forever alone", "lone");
	ZombieEvent	zmbwalker("walker");
	ZombieEvent	zmbrunner("runner");
	ZombieEvent	zmbfucker("fucker");

	zombieAlone.announce();
	Zombie* z1 = zmbfucker.randomChump();
	Zombie* z2 = zmbrunner.randomChump();
	Zombie* z3 = zmbfucker.randomChump();
	Zombie* z4 = zmbwalker.randomChump();
	Zombie* z5 = zmbrunner.randomChump();
	Zombie* z6 = zmbfucker.randomChump();
	
	delete z1;
	delete z2;
	delete z3;
	delete z4;
	delete z5;
	delete z6;
	return (0);
}
