/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:52:44 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:52:47 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

int		main() {
	const std::string	str("HI THIS IS BRAIN");
	const std::string	*ptr = &str;
	const std::string	&ref = str;

	std::cout << "pointer: " << *ptr << std::endl;
	std::cout << "reference: " << ref << std::endl;
	return (0);
}
