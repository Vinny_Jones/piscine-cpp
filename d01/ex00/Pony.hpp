/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 20:56:43 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 20:56:47 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_HPP
# define PONY_HPP

#include <iostream>

class Pony {
public:
	Pony();
	Pony(const char *name);
	Pony(const Pony &copy);
	virtual ~Pony();
	Pony	&operator=(const Pony &assign);

	std::string		ponyName;
};

Pony	*ponyOnTheStack();
Pony	*ponyOnTheHeap();

#endif
