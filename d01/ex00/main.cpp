/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 20:53:39 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 20:54:41 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

void	test();

int 	main() {
	test();
	return (0);
}

void	test() {
	Pony	*stackPony = ponyOnTheStack();
	Pony	*heapPony = ponyOnTheHeap();

	std::cout << "Stack pony name in main = " << stackPony->ponyName << std::endl;
	std::cout << "Heap pony name in main = " << heapPony->ponyName << std::endl;

	delete heapPony;
}