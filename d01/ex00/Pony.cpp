/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 20:56:55 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 20:56:57 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

Pony::Pony() {}

Pony::Pony(const char *name) : ponyName(name) {}

Pony::Pony(const Pony &copy) {
	*this = copy;
}

Pony::~Pony() {
	ponyName.clear();
}

Pony& Pony::operator=(const Pony &assign) {
	ponyName = assign.ponyName;
	return (*this);
}

Pony	*ponyOnTheHeap() {
	Pony	*heapGuy = new Pony("heap pony");

	std::cout << "Created one " << heapGuy->ponyName << std::endl;
	return (heapGuy);
}

Pony	*ponyOnTheStack() {
	Pony	stackGuy("stack pony");
	Pony	*returnPointer = &stackGuy;

	std::cout << "Created one " << stackGuy.ponyName << std::endl;
	return (returnPointer);
}
