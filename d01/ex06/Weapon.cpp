/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 22:08:48 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 22:08:50 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.hpp"

Weapon::Weapon(const std::string &str) : m_type(str) {}

Weapon::Weapon(const Weapon &copy) {
	*this = copy;
}

Weapon::~Weapon() {}

Weapon	&Weapon::operator=(const Weapon &assign) {
	m_type = assign.m_type;
	return (*this);
}

const std::string	&Weapon::getType() {
	return (m_type);
}

void	Weapon::setType(const std::string &newType) {
	m_type = newType;
}
