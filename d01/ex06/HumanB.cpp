/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 22:08:30 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 22:08:31 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.hpp"

HumanB::HumanB(const std::string &name) : m_name(name) {
	m_weapon = NULL;
}

HumanB::HumanB(const HumanB &copy) {
	*this = copy;
}

HumanB::~HumanB() {}

HumanB	&HumanB::operator=(const HumanB &assign) {
	m_name = assign.m_name;
	m_weapon = assign.m_weapon;
	return (*this);
}

void	HumanB::setWeapon(Weapon &weapon) {
	m_weapon = &weapon;
}

void	HumanB::attack() const {
	if (m_weapon)
		std::cout << m_name << " attacks with his " << m_weapon->getType() << std::endl;
}
