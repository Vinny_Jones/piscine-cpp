/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 22:08:40 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 22:08:42 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WEAPON_HPP
# define WEAPON_HPP

# include <iostream>

class Weapon {
public:
	Weapon(const std::string &str = "default");
	Weapon(const Weapon &copy);
	virtual ~Weapon();
	Weapon	&operator=(const Weapon &assign);

	const std::string	&getType();
	void				setType(const std::string &newType);
private:
	std::string	m_type;
};

#endif
