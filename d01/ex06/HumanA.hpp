/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 22:07:59 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 22:08:01 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANA_HPP
# define HUMANA_HPP

# include "Weapon.hpp"

class HumanA {
public:
	HumanA(const std::string &name, Weapon &weapon);
	HumanA(const HumanA &copy);
	virtual ~HumanA();
	HumanA	&operator=(const HumanA &assign);

	void	attack() const;
private:
	std::string	m_name;
	Weapon		&m_weapon;
};

#endif
