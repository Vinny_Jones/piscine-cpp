/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 22:08:08 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 22:08:10 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.hpp"

HumanA::HumanA(const std::string &name, Weapon &weapon) : m_name(name), m_weapon(weapon) {}

HumanA::HumanA(const HumanA &copy) : m_weapon(copy.m_weapon) {
	*this = copy;
}

HumanA::~HumanA() {}

HumanA& HumanA::operator=(const HumanA &assign) {
	m_weapon = assign.m_weapon;
	m_name = assign.m_name;
	return (*this);
}

void	HumanA::attack() const {
	std::cout << m_name << " attacks with his " << m_weapon.getType() << std::endl;
}
