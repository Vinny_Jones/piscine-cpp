/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 22:08:20 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 22:08:22 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANB_HPP
# define HUMANB_HPP

# include "Weapon.hpp"

class HumanB {
public:
	HumanB(const std::string &name = "noname");
	HumanB(const HumanB &copy);
	virtual  ~HumanB();
	HumanB	&operator=(const HumanB &assign);

	void	attack() const;
	void	setWeapon(Weapon &weapon);
private:
	std::string	m_name;
	Weapon		*m_weapon;
};

#endif
