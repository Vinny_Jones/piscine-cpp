/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:31:36 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:31:39 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

int		main(int argc, const char *argv[]) {
	ZombieHorde	newHorde(argc > 1 ? (unsigned int)atoi(argv[1]) : 45);
	ZombieHorde	defaultHorde;

	newHorde.announce();
	defaultHorde.announce();
	defaultHorde = newHorde;
	defaultHorde.announce();
	return (0);
}
