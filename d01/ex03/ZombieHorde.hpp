/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:37:29 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:37:31 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEHORDE_HPP
# define ZOMBIEHORDE_HPP

# include "Zombie.hpp"
# include <cstdlib>

class ZombieHorde : public Zombie {
public:

	ZombieHorde(unsigned int amount = 0);
	ZombieHorde(const ZombieHorde &copy);
	virtual ~ZombieHorde();
	ZombieHorde	&operator=(const ZombieHorde &assign);

	virtual void	announce() const;
private:
	unsigned int	m_amount;
	Zombie*			m_horde;
};

#endif
