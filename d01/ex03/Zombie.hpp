/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:13:57 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:14:05 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

#include <iostream>

class Zombie {
public:
	static const std::string Names[25];

	Zombie(std::string name = "[noname]", std::string type = "walker");
	Zombie(const Zombie &copy);
	virtual ~Zombie();
	Zombie	&operator=(const Zombie &assign);

	virtual void	announce() const;
	void    setName(const std::string &name);
	void    setType(const std::string &type);
protected:
	std::string	m_type;
	std::string	m_name;
};

#endif
