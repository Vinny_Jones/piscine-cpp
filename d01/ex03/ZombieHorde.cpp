/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:37:38 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:37:41 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(unsigned int amount) : m_amount(amount) {
	try {
		m_horde = (m_amount) ? new Zombie[m_amount] : NULL;
	} catch (std::bad_alloc &bad) {
		std::cerr << "Error while allocatin memory for " << m_amount << " guys: " << bad.what() << std::endl;
		m_horde = NULL;
		m_amount = 0;
		return ;
	}

	for (unsigned int i = 0; i < m_amount; ++i) {
		srand (time(NULL) + i);
		m_horde[i].setName(Zombie::Names[rand() % 25]);
		m_horde[i].setType("horde member");
	}
}

ZombieHorde::ZombieHorde(const ZombieHorde &copy) {
	m_horde = NULL;
	*this = copy;
}

ZombieHorde::~ZombieHorde() {
	if (m_horde)
		delete[] m_horde;
}

ZombieHorde	&ZombieHorde::operator=(const ZombieHorde &assign) {
	if (m_horde)
		delete[] m_horde;
	m_amount = assign.m_amount;
	try {
		m_horde = (m_amount) ? new Zombie[m_amount] : NULL;
	} catch (std::bad_alloc &bad) {
		std::cerr << "Error while allocatin memory for " << m_amount << " guys: " << bad.what() << std::endl;
		m_horde = NULL;
		m_amount = 0;
		return (*this);
	}
	for (unsigned int i = 0; i < m_amount; ++i) {
		m_horde[i] = assign.m_horde[i];
	}

	return (*this);
}

void ZombieHorde::announce() const {
	for (unsigned int i = 0; i < m_amount; ++i) {
		m_horde[i].announce();
	}
}
