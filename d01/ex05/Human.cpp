/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:54:08 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:54:10 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

Human::Human() {}

Human::Human(const Human &copy) : m_brain(copy.m_brain) {
	*this = copy;
}

Human::~Human() {}

Human& Human::operator=(const Human &) {
	return (*this);
}

std::string	Human::identify() const {
	return (m_brain.identify());
}

const Brain	&Human::getBrain() {
	return (m_brain);
}
