/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:53:45 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:53:48 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"

Brain::Brain() {}

Brain::Brain(const Brain &) {}

Brain::~Brain() {}

Brain	&Brain::operator=(const Brain &) {
	return (*this);
}

std::string	Brain::identify() const {
	std::stringstream	strstream;
	strstream << std::hex << this;

	std::string result = strstream.str();
	for (size_t i = 2; i < result.length(); i++)
		result[i] = std::toupper(result[i]);
	return (result);
}
