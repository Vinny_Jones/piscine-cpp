/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 21:53:56 by apyvovar          #+#    #+#             */
/*   Updated: 2018/01/16 21:53:59 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMAN_HPP
# define HUMAN_HPP

# include "Brain.hpp"

class Human {
public:
	Human();
	Human(const Human &copy);
	virtual ~Human();
	Human	&operator=(const Human &);

	std::string	identify() const;
	const Brain	&getBrain();
private:
	const Brain	m_brain;
};

#endif
